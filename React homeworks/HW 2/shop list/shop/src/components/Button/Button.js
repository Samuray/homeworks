import { Component } from "react";
import PropTypes from "prop-types";

 class Button extends Component {
  render() {
    return (
      <button
        onClick={this.props.click}
        className={`${this.props.size} ${this.props.color} waves-effect btn`}
      >
        {this.props.icon}
        {this.props.text}
      </button>
    );
  }
}
Button.propTypes = {
  click: PropTypes.func,
  size: PropTypes.string,
  color: PropTypes.string,
  icon: PropTypes.object,
  text: PropTypes.string,
};
Button.defaultProps = {
  color: "blue",
  icon: null,
};

export default Button;
