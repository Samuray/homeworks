const url = 'https://www.boredapi.com/api/activity';

const button = document.querySelector("button");
button.addEventListener("click", () => {
 const paragraph = document.querySelector(".activity");
paragraph.innerHTML = "Loading..."
 const xhr = new XMLHttpRequest();
 xhr.open("GET", url);
 xhr.send();
 xhr.onload = () => {
if (xhr.status === 200) {
 const { activity } = JSON.parse(xhr.response);
 paragraph.innerHTML = activity;
}else {
 console.error(`Bad request! Status:${xhr.response}`)
  }
}
 xhr.onerror = () => {
  console.error(`Bad request! Status: ${xhr.status}`);    }
});
