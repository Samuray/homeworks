import {setCookie} from "./setCookie.js";

export const setToken = async () => {
    const {data: {token}, status} = await axios.get('http://localhost:3000/');
    if( status === 200 ) {
        setCookie('token', token, {
            'max-age': 60*15
        });
    }
}
