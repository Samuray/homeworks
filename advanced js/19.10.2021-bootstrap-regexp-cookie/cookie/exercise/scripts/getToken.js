export const getToken = (name) => {
    const result = document.cookie.split('; ');
    const token = result.find(e => e.includes(name))
    return token.split('=')[1];
}
