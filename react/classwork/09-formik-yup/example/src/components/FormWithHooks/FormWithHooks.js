import React from 'react';

const FormWithHooks = () => {
    const handleSubmit = (e) => {
        e.preventDefault();
    }

    return (
        <form onSubmit={(e) => handleSubmit(e)}>
            <h3>HOOKS</h3>
            <input
                type="text"
                name="name"
                placeholder="Name"
            />

            <input
                type="text"
                name="email"
                placeholder="Email"
            />

            <input
                type="password"
                name="password"
                placeholder="Password"
            />

            <button type="submit">Submit</button>
        </form>
    )
}

export default FormWithHooks;