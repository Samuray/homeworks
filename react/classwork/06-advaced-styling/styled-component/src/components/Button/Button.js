
const styles = `
  padding: 6px 10px;
  box-sizing: border-box;
  background: #61dafb;
  color: #fff;
  outline: unset;
  filter: brightness(.9);
  cursor: pointer;
  margin: 10px;
`;

