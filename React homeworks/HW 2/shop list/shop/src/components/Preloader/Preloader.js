import { Component } from "react";
import style from "./Preloader.module.scss";

 class Preloader extends Component {
  render() {
    return (
      <div className={style.preloader}>
        <div className={style.loader}> </div>
      </div>
    );
  }
}

export default Preloader;
