import { INIT_CARDS, IS_FAVORITE } from "../actions/cardItemsActions";

const initialValues = {
    cards: [],
}

const cardItemsReducer = (state = initialValues, action) => {
    switch (action?.type){
        case INIT_CARDS: {
            console.log(action.payload);
            return {...state, cards:action.payload}
        }
        case IS_FAVORITE: {
            const newCards = [...state.cards];
            const index = newCards.findIndex(elem => elem.id === action.payload.id);
            if (index !== -1) {
                newCards[index] = action.payload
            }

            return {...state, cards: newCards}
        }
        default:
            return state
    }

}

export default cardItemsReducer;