"use strict";

let tab = function () {
    let tabNav = document.querySelectorAll(".tabs-nvg"),
        tabContent = document.querySelectorAll(".tab"),
        tabName;

    tabNav.forEach((item) => {
        item.addEventListener("click", selectTabNav);
    });
    function selectTabNav() {
        tabNav.forEach((item) => {
            item.classList.remove("is-active");
        });
        this.classList.add("is-active");
        tabName = this.getAttribute("data-tab-name");
        selectTabContent(tabName);
    }
    function selectTabContent(tabName) {
        tabContent.forEach((item) => {
            item.classList.contains(tabName)
                ? item.classList.add("is-active")
                : item.classList.remove("is-active");
        });
    }
};
tab();


$(function() {
    $('.img-section').hide().slice(0,12).show();
    // $('.img-section').slice(0,12).show();
    $(".load-more-btn").on ("click", function (e){
        $('.img-section').show()
        e.preventDefault();
        let $this = $(this);
        setTimeout(function() {
            $this.slideUp(1500);
        }, 2000);
    });
    $('.other-blocks-menu-nav').on('click', function() {
        let $category = $(this).attr('id');
        if($category === 'all-img-section' ) {
            $('.img-section').addClass('hide');
            setTimeout(function() {
                $('.img-section').removeClass('hide');
            }, 300);
        }
        else {
            $('.img-section').addClass('hide');
            setTimeout(function() {
                $('.' + $category).removeClass('hide');
            }, 300)
        }
    });
});


$(function() {
    $('.text-description').hide().first().show();
    $('.team-member').hide().first().show();
    $('.member-photo').hide().first().show();


    $('.slider-photo').on("click",function(){
        $(this).addClass('active').siblings().removeClass('active');
        switchContent();
    })

    $('.slider-arrow.right').on("click",function(){
        let activeIndex = $('.slider-photo.active').index() - 1;
        let newIndex = (activeIndex === 3) ? 0 : activeIndex + 1;
        $(`.slider-photo:eq(${newIndex})`).addClass('active').siblings().removeClass('active');
        switchContent();
    })

    $('.slider-arrow.left').on("click",function(){
        let activeIndex = $('.slider-photo.active').index() - 1;
        let newIndex = (activeIndex === 0) ? 3 : activeIndex - 1;
        $(`.slider-photo:eq(${newIndex})`).addClass('active').siblings().removeClass('active');
        switchContent();
    })

    function switchContent () {
        let activeIndex =  $('.slider-photo.active').index() - 1;
        $(`.text-description:eq(${activeIndex})`).show().siblings('.text-description').hide();
        $(`.team-member:eq(${activeIndex})`).show().siblings('.team-member').hide();
        $(`.member-photo:eq(${activeIndex})`).show().siblings('.member-photo').hide();
    }
})