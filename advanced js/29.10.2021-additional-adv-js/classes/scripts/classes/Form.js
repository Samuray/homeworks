import {
    htmlTemplateParagraph,
    htmlTemplateButton,
    htmlTemplateInput,
    htmlTemplateImage
} from "../constants/htmlTemplates.js";

export class Form {
    constructor() {
        this.mappedTemplates = {
            select: '',
            paragraph: htmlTemplateParagraph,
            button: htmlTemplateButton,
            input: htmlTemplateInput,
            image: htmlTemplateImage,
        }
        this.form = document.createElement('form');
        this.select = document.createElement('select');
        this.inputsWrapper = document.createElement('div');
        this.selectOption = `
        <option value="select">Выберете элемент</option>
        <option value="paragraph">Параграф</option>
        <option value="button">Кнопка</option>
        <option value="input">Инпут</option>
        <option value="image">Изображение</option>
        `;
    }

    createElements() {
        this.select.innerHTML = this.selectOption;
        this.form.innerHTML = `<h3>Choose element creator</h3>`
        this.form.append(this.select);
        this.select.addEventListener('change', this.selectOnChange.bind(this))
        this.form.append(this.inputsWrapper);
    }

    getSelectValue() {
        return this.select.value;
    }

    selectOnChange() {
         this.inputsWrapper.innerHTML = this.mappedTemplates[this.select.value];
    }

    getInputsValues() {
        const values = {};
        this.inputsWrapper.querySelectorAll('input').forEach( ({name, value}) => {
            values[name] = value;
        } );
        return values;
    }

    render() {
        this.createElements();
        return this.form;
    }
}
