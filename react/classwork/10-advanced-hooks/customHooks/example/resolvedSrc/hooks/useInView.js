import {useState, useEffect, useRef} from 'react';

const useInView = () => {
    const [inView, setInView] = useState(false);
    const itemRef = useRef(null);
    const debounceRef = useRef(false);

    const setInViewFunc = () => {
        if(itemRef.current && !debounceRef.current) {
            const { top } = itemRef.current.getBoundingClientRect();
            if (!top) return;

            debounceRef.current = true;
            if (top <= window.scrollY && !inView) {
                setInView(true);
            }
            setTimeout(() => debounceRef.current = false, 50)
        }
    }

    useEffect(() => {
        window.addEventListener('scroll', setInViewFunc, { passive: true });

        return () => {
            window.addEventListener('scroll', setInViewFunc);
        }
    }, [itemRef]);

    return [itemRef, inView];
};

export default useInView;