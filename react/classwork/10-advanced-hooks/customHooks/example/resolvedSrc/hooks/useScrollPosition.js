import {useState, useEffect, useRef} from "react";

const useScrollPosition = (timeout = 100) => {
    const [position, setPosition] = useState(0);
    const debounceRef = useRef(false);

    const setScrollPosition = () => {
        if (!debounceRef.current) {
            debounceRef.current = true;
            setPosition(Math.floor(window.scrollY));
            setTimeout(() => debounceRef.current = false, timeout)
        }
    };

    useEffect(() => {
        window.addEventListener('scroll', setScrollPosition, { passive: true });

        return () => {
            window.removeEventListener('scroll', setScrollPosition);
        }
    }, [])

    return position;
};

export default useScrollPosition ;