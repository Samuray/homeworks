import React, {useEffect, useState} from "react";
import './App.scss';
import User from "./components/User";
import useDeviseType from './hooks/useDeviseType';

const users = [
  { id: 1, name: 'Anonymous', avatar: `https://i.pravatar.cc/1000?v=${Math.random()}`},
  { id: 2, name: 'Anonymous', avatar: `https://i.pravatar.cc/1000?v=${Math.random()}`},
  { id: 3, name: 'Anonymous', avatar: `https://i.pravatar.cc/1000?v=${Math.random()}`},
  { id: 4, name: 'Anonymous', avatar: `https://i.pravatar.cc/1000?v=${Math.random()}`},
  { id: 5, name: 'Anonymous', avatar: `https://i.pravatar.cc/1000?v=${Math.random()}`},
  { id: 6, name: 'Anonymous', avatar: `https://i.pravatar.cc/1000?v=${Math.random()}`},
  { id: 7, name: 'Anonymous', avatar: `https://i.pravatar.cc/1000?v=${Math.random()}`},
  { id: 8, name: 'Anonymous', avatar: `https://i.pravatar.cc/1000?v=${Math.random()}`},
  { id: 9, name: 'Anonymous', avatar: `https://i.pravatar.cc/1000?v=${Math.random()}`},
  { id: 10, name: 'Anonymous', avatar: `https://i.pravatar.cc/1000?v=${Math.random()}`},
  { id: 11, name: 'Anonymous', avatar: `https://i.pravatar.cc/1000?v=${Math.random()}`},
  { id: 12, name: 'Anonymous', avatar: `https://i.pravatar.cc/1000?v=${Math.random()}`},
  { id: 13, name: 'Anonymous', avatar: `https://i.pravatar.cc/1000?v=${Math.random()}`},
  { id: 14, name: 'Anonymous', avatar: `https://i.pravatar.cc/1000?v=${Math.random()}`},
  { id: 15, name: 'Anonymous', avatar: `https://i.pravatar.cc/1000?v=${Math.random()}`},
]

function App() {
  const type = useDeviseType();

  const [length, setLength] = useState(2);

  useEffect(() => {
    if(type === 'mobile') {
      setLength(2);
    } else if(type === 'tablet') {
      setLength(4);
    } else if(type === 'desktop') {
      setLength(6);
    }
  }, [type]);



  return (
    <div className="App">
      <h1>USERS</h1>

      <div style={{ display: 'flex', flexWrap: 'wrap'}}>
        {users.slice(0, length).map(({ id, name, avatar }) => <User key={id} id={id} name={name} avatar={avatar} />)}
      </div>
    </div>
  );
}

export default App;
