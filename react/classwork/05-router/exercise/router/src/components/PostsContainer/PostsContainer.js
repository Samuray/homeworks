import React from 'react';
import Post from "../Post";
import styles from './PostsContainer.module.scss';
import Preloader from "../Preloader";
import GoBack from "../GoBack";

const PostsContainer = (props) => {
        const { posts, isLoading, id, history } = props;

        return (
              <section className={styles.root}>
                  <GoBack history={history}/>
                      <div className={styles.postsContainer}>
                          {isLoading
                              ? <Preloader color="secondary" size={60} />
                              : <>{posts.map(({id, ...args}) => <Post history={history} key={id} id={id} {...args} />)}</>}
                      </div>
              </section>
        );
};

export default PostsContainer;
