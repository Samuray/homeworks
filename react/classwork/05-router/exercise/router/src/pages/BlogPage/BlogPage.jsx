import React, {useEffect, useState} from "react";
import PostsContainer from "../../components/PostsContainer";
import GoBack from "../../components/GoBack";

const BlogPage = (props) => {
    const [posts, setPosts] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);



    useEffect(() => {
        (async ()=>{
            try {
                setIsLoading(true)

                const response = await fetch('https://ajax.test-danit.com/api/json/posts')
                    .then(e => e.json())

                setPosts(response);
                setIsLoading(false);


            } catch (e) {
                setIsLoading(false);
                setIsError(true);
            }
        })()
    },[]);

    const { history } = props;

    return (
        <>

        <PostsContainer posts={posts} isLoading={isLoading} isError={isError} history={history} />

        </>)
}

export default BlogPage;