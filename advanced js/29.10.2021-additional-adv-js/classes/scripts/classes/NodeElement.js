 class NodeElement {
    constructor(id, classElem) {
        this.id = id;
        this.classElem = classElem;
        this.element = null;
    }

    render(){
        return this.element;
    }
}

 export class  Paragraph extends NodeElement {
     /**
      * @param values
      */
     constructor(values) {
         console.log(values)
         const {id, classes: classElem, text} = values;
         super(id, classElem)
         this.text = text;
     }
     createElement(){
         this.element = document.createElement("p");
         if (this.classElem) this.element.classList.add(this.classElem);
         if (this.id) this.element.id = this.id;
         this.element.innerText = this.text;
     }
     render(){
         this.createElement();
         return super.render();
     }
 }

 export class  Image extends NodeElement {
     /**
      * @param values {object}
      */
     constructor(values) {
         const {id, classes: classElem, src, alt, width, height} = values;
         super(id, classElem)
         this.src=src;
         this.alt=alt;
         this.width=width;
         this.height=height;

     }
     createElement(){
         this.element = document.createElement("img");
         this.element.classList.add(this.classElem);
         this.element.id = this.id;
         this.element.src = this.src;
         this.element.alt = this.alt;
         this.element.width = this.width;
         this.element.height = this.height;

     }
     render(){
         this.createElement();
         return super.render();
     }
 }
