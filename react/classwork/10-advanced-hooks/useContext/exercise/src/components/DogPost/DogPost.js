import React, {useContext} from 'react';
import PropTypes from "prop-types";
import LangContext from "../../contex/LangContext";

const DogPost = () => {

    const { value } = useContext(LangContext);

    return (
              <>
                  <img width={480} height={360} style={{ backgroundColor: 'lightgray' }} src="./DJ_Dog.gif" alt="DJ DOG"/>
                  { value === 'EN' && <p>It's an edited picture of the dog (Indian Spitz) who is rocking on DJ.</p> }
                  { value === 'DE' && <p>Es ist ein bearbeitetes Bild des Hundes (Indian Spitz), der auf DJ rockt.</p> }
                  { value === 'FR' && <p>C'est une photo retouchée du chien (Indian Spitz) qui se balance sur DJ.</p> }
                  { value === 'UA' && <p>Це відредагований малюнок собаки (індійського шпіца), який качає на DJ.</p> }
              </>
        );
};

export default DogPost;
