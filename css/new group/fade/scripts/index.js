const autoSlider = {
    images: [
        'images/1.jpg',
        'images/2.jpg',
        'images/3.jpg',
        'images/4.jpg',
        'images/5.jpg',
    ],
    titles: [
        'The greatest glory',
        'Your time is limited',
        'If life were predictable',
        'If you set your goals',
        'Life is what happens',
    ],
    interval: null,
    currentSlide: 0,
    container: document.querySelector('.container'),
    imageElem: document.querySelector('.image-container img'),
    textElem: document.querySelector('.text-container'),

    start (){
        this.change();
    this.interval = setInterval(this.change.bind(this), 3000)

    },
  change() {

        if (this.currentSlide < images.length-1) {
            this.currentSlide++;
            this.container.classList.add('fade-in')
            this.imageElem.src = this.images[this.currentSlide];
            this.textElem.innerText = this.titles[this.currentSlide];
            this.container.classList.add("fade-out");
            setTimeout(() => {
                this.container.classList.remove("fade-out");}, 1500
            );
        }
        else {
            clearInterval(this.interval);
        }
  },






};
autoSlider.start();