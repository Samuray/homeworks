
import React, {useEffect, useState} from "react";
import {BrowserRouter as Router, Switch, Route,} from "react-router-dom";
import style from "./App.module.scss";
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize";
import Navbar from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import List from "./components/List/List";
import Preloader from "./components/Preloader/Preloader";
import Empty from "./components/Empty/Empty";
import CartList from "./components/CartList/CartList";
import Favorites from "./components/Favorites/Favorites";


 const App = () =>  {
     const [cards, setCards] = useState([]);
     const [products, setProducts] = useState(JSON.parse(localStorage.getItem("products")) || []);
     const [error, setError] = useState(null);
     const [loading, setLoading] = useState(false);
     const [cart, setCart] = useState(JSON.parse(localStorage.getItem("cart")) || []);
     const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem("favorites")) || []);

     useEffect(() => {
         setLoading(true)
         fetch("product.json")
             .then((res) => {
                 if (res.ok) {
                     return res.json();
                 }
                 throw new Error("Failed to load");
             })
             .then((product) => setCards(product) || setLoading(false))
             .catch((e) => setError(e.message)|| setLoading(false));
     }, []);

     const addToCart = (id) => {
         const cartUpdate = products.map(e => e.id === id ? {...e, cart:{inCart: "true",count: e.cart.count+1}} : e)
         setProducts(cartUpdate)
         localStorage.setItem("products", JSON.stringify(cartUpdate));
         const isCart = cartUpdate.filter(e => e.cart.inCart==="true")
         setCart(isCart)
         localStorage.setItem("cart", JSON.stringify(isCart));
     };

     const removeOneCart=(id)=> {
         const cartUpdate = products.map(e => e.id === id ? {...e, cart:{inCart: "true",count: e.cart.count-1}} : e)
         setProducts(cartUpdate)
         localStorage.setItem("products", JSON.stringify(cartUpdate));
         const isCart = cartUpdate.filter(e => e.cart.inCart==="true")
         setCart(isCart)
         localStorage.setItem("cart", JSON.stringify(isCart));
     }

     const removeFullCart=(id)=> {
         const cartUpdate = products.map(e => e.id === id ? {...e, cart:{inCart: false ,count: 0}} : e)
         setProducts(cartUpdate)
         localStorage.setItem("products", JSON.stringify(cartUpdate));
         const isCart = cartUpdate.filter(e => e.cart.inCart==="true")
         setCart(isCart)
         localStorage.setItem("cart", JSON.stringify(isCart));
     }

     const isFavorite = (id) => {
         const cartUpdate = products.map(e => e.id === id ? {...e,isFavourite : !e.isFavourite} : e)
         setProducts(cartUpdate)
         localStorage.setItem("products", JSON.stringify(cartUpdate));
         const isFavorite = cartUpdate.filter(e => e.isFavourite===true)
         setFavorites(isFavorite)
         localStorage.setItem("favorites", JSON.stringify(isFavorite));
     };

     const renderContainer = () => {
         if (error) {
             return <div>{error}</div>;
         }
         if (loading) {
             return <Preloader />;
         }
         if (cards.length === 0) {
             return <div>{"Product list empty!!!"}</div>;
         }
         if (cards) {
             if (!products.length){
                 setProducts(cards)
             }
             return <List
                 productList={products}
                 addToCart={addToCart}
                 isFavorite={isFavorite}
             />;
         }
         return null;
     };

     return (
         <Router>
             <div className={style.App}>
                 <header>
                     <Navbar />
                 </header>
                 <main className={style.main}>
                     <Switch>
                         <Route exact path="/">
                             {renderContainer()}
                         </Route>
                         <Route path="/cart">
                             {!cart.length ?
                                 <Empty text={'Your basket is empty'}/>
                                 :
                                 <CartList
                                     cartList={cart}
                                     addCart={addToCart}
                                     removeOneCart={removeOneCart}
                                     removeFullCart={removeFullCart}
                                 />
                             }
                         </Route>
                         <Route path="/favorites">
                             {!favorites.length ?
                                 <Empty text={'Your favorites list is empty'}/>
                                 :
                                 <List
                                     productList={favorites}
                                     addToCart={addToCart}
                                     isFavorite={isFavorite}
                                 />}
                             ;
                         </Route>
                     </Switch>
                 </main>
                 <footer>
                     <Footer />
                 </footer>
             </div>
         </Router>
     );

 }

export default App;
