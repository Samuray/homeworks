import './App.scss';
import Container from "./components/Container";
import Form from './components/Form';

function App() {
  return (
    <div className="App">
        {/*<Container />*/}
        <Form />
    </div>
  );
}

export default App;
