const btn = document.querySelector('.header__burger-btn');
const menu = document.querySelector('.navbar__menu');

btn.addEventListener('click', () => {
  menu.classList.toggle('navbar__menu--is-active');
})