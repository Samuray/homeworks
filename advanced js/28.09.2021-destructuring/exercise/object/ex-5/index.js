/*
Напишите функцию, которая принимает в себя информацию об пользователе
в виде объекта, среди параметров которого - поля age, и wantLicense,
и которая возвращает ответ - может ли человек получить
в его возрасте желаемые права.
В ответе должен быть собственно ответ: да/нет, категория, возраст пользователя, минимальный необходимый возраст;

Используйте массив объектов, описывающий разные типы водительских прав.
*/


const driverLicence = [{
    age: 16,
    type: "A1",
    time: 2
}, {
    age: 16,
    type: "A2",
    time: 2
},
    {
        age: 18,
        type: "B1",
        time: 6
    },
    {
        age: 18,
        type: "B",
        time: 4
    },
    {
        age: 19,
        type: "BE",
        time: 4
    },
    {
        age: 21,
        type: "D1",
        time: 3
    }
];


const users = [{
    id: 1,
    name: "Leanne Graham",
    wantLicense: "B1",
    email: "Sincere@april.biz",
    address: {
        street: "Kulas Light",
        suite: "Apt. 556",
        city: "Gwenborough",
        zipcode: "92998-3874",
        geo: {"lat": "-37.3159", "lng": "81.1496"}
    },
    phone: "1-770-736-8031 x56442",
    age: 12,
    company: {
        name: "Romaguera-Crona",
        catchPhrase: "Multi-layered client-server neural-net",
        bs: "harness real-time e-markets"
    }
}, {
    id: 2,
    name: "Ervin Howell",
    wantLicense: "B2",
    email: "Shanna@melissa.tv",
    address: {
        street: "Victor Plains",
        suite: "Suite 879",
        city: "Wisokyburgh",
        zipcode: "90566-7771",
        geo: {"lat": "-43.9509", "lng": "-34.4618"}
    },
    phone: "010-692-6593 x09125",
    age: 21,
    company: {
        name: "Deckow-Crist",
        catchPhrase: "Proactive didactic contingency",
        bs: "synergize scalable supply-chains"
    }
}, {
    id: 3,
    name: "Clementine Bauch",
    wantLicense: "A1",
    email: "Nathan@yesenia.net",
    address: {
        street: "Douglas Extension",
        suite: "Suite 847",
        city: "McKenziehaven",
        zipcode: "59590-4157",
        geo: {"lat": "-68.6102", "lng": "-47.0653"}
    },
    phone: "1-463-123-4447",
    age: 16,
    company: {
        name: "Romaguera-Jacobson",
        catchPhrase: "Face to face bifurcated interface",
        bs: "e-enable strategic applications"
    }
}, {
    id: 4,
    name: "Patricia Lebsack",
    wantLicense: "D1",
    email: "Julianne.OConner@kory.org",
    address: {
        street: "Hoeger Mall",
        suite: "Apt. 692",
        city: "South Elvis",
        zipcode: "53919-4257",
        geo: {"lat": "29.4572", "lng": "-164.2990"}
    },
    phone: "493-170-9623 x156",
    age: 34,
    company: {
        name: "Robel-Corkery",
        catchPhrase: "Multi-tiered zero tolerance productivity",
        bs: "transition cutting-edge web services"
    }
}, {
    id: 5,
    name: "Chelsey Dietrich",
    wantLicense: "D1",
    email: "Lucio_Hettinger@annie.ca",
    address: {
        street: "Skiles Walks",
        suite: "Suite 351",
        city: "Roscoeview",
        zipcode: "33263",
        geo: {"lat": "-31.8129", "lng": "62.5342"}
    },
    phone: "(254)954-1289",
    age: 17,
    company: {
        name: "Keebler LLC",
        catchPhrase: "User-centric fault-tolerant solution",
        bs: "revolutionize end-to-end systems"
    }
}, {
    id: 6,
    name: "Mrs. Dennis Schulist",
    wantLicense: "A2",
    email: "Karley_Dach@jasper.info",
    address: {
        street: "Norberto Crossing",
        suite: "Apt. 950",
        city: "South Christy",
        zipcode: "23505-1337",
        geo: {"lat": "-71.4197", "lng": "71.7478"}
    },
    phone: "1-477-935-8478 x6430",
    age: 22,
    company: {
        name: "Considine-Lockman",
        catchPhrase: "Synchronised bottom-line interface",
        bs: "e-enable innovative applications"
    }
}, {
    id: 7,
    name: "Kurtis Weissnat",
    wantLicense: "B1",
    email: "Telly.Hoeger@billy.biz",
    address: {
        street: "Rex Trail",
        suite: "Suite 280",
        city: "Howemouth",
        zipcode: "58804-1099",
        geo: {"lat": "24.8918", "lng": "21.8984"}
    },
    phone: "210.067.6132",
    age: 18,
    company: {
        name: "Johns Group",
        catchPhrase: "Configurable multimedia task-force",
        bs: "generate enterprise e-tailers"
    }
}, {
    id: 8,
    name: "Nicholas Runolfsdottir V",
    wantLicense: "B1",
    email: "Sherwood@rosamond.me",
    address: {
        street: "Ellsworth Summit",
        suite: "Suite 729",
        city: "Aliyaview",
        zipcode: "45169",
        geo: {"lat": "-14.3990", "lng": "-120.7677"}
    },
    phone: "586.493.6943 x140",
    age: 16,
    company: {
        name: "Abernathy Group",
        catchPhrase: "Implemented secondary concept",
        bs: "e-enable extensible e-tailers"
    }
}, {
    id: 9,
    name: "Glenna Reichert",
    wantLicense: "D1",
    email: "Chaim_McDermott@dana.io",
    address: {
        street: "Dayna Park",
        suite: "Suite 449",
        city: "Bartholomebury",
        zipcode: "76495-3109",
        geo: {"lat": "24.6463", "lng": "-168.8889"}
    },
    phone: "(775)976-6794 x41206",
    age: 34,
    company: {
        name: "Yost and Sons",
        catchPhrase: "Switchable contextually-based project",
        bs: "aggregate real-time technologies"
    }
}, {
    id: 10,
    name: "Clementina DuBuque",
    wantLicense: "A2",
    email: "Rey.Padberg@karina.biz",
    address: {
        street: "Kattie Turnpike",
        suite: "Suite 198",
        city: "Lebsackbury",
        zipcode: "31428-2261",
        geo: {"lat": "-38.2386", "lng": "57.2232"}
    },
    phone: "024-648-3804",
    age: 24,
    company: {
        name: "Hoeger LLC",
        catchPhrase: "Centralized empowering task-force",
        bs: "target end-to-end models"
    }
}]
