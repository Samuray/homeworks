
import React from 'react';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import style from './App.module.scss'

 class App extends React.Component {
    state = {
        firstModalActive: false,
        secondModalActive: false
    }

    firstModal = () => {
        this.setState({
            firstModalActive: !this.state.firstModalActive
        })
    }

    secondModal = () => {
        this.setState({
            secondModalActive: !this.state.secondModalActive
        })
    }

    render() {
        return (
            <div className={style.container}>
                <div className={style.buttonGroup}>
                    <Button
                        onClick={this.firstModal}
                        backgroundColor={"green"}
                        text={"Open first modal"}
                    />

                    <Button
                        onClick={this.secondModal}
                        backgroundColor={"blue"}
                        text={"Open second modal"}
                    />
                </div>
                {this.state.firstModalActive &&
                <Modal
                    header={' Do you want to delete this file?'}
                    closeButton={true}
                    isOpen={this.firstModal}
                    textmodal={"Once you delete this file, it won't be possible to undo this action.  Are you sure you want to delete it?"}
                    actions={
                        <div className={style.modalFooter}>
                            <Button text={"Ok"}/>
                            <Button text={"Cancel"} onClick={this.firstModal}/>
                        </div>}
                />}
                {this.state.secondModalActive &&
                <Modal
                    header={'Well, is it interesting for you here?'}
                    closeButton={false}
                    isOpen={this.secondModal}
                    textmodal={"You can click as much as you like with these buttons. This is just a test!"}
                    actions={
                        <div className={style.modalFooter}>
                            <Button backgroundColor={"grey"} text={"Yes"}/>
                            <Button backgroundColor={"grey"} text={"No"} onClick={this.secondModal}/>
                        </div>}
                />}
            </div>
        );
    }
}

export default App;