import { combineReducers } from "redux";
import userReducer from "./userReducer";
import cardItemsReducer from "./cardItemsReducer";
import cartReducer from "./cartReducer";
import modalReducer from "./modalReducer";

const appReducer = combineReducers({
    user: userReducer,
    cards:cardItemsReducer,
    cartItems: cartReducer,
    modal: modalReducer,
})

export default appReducer;