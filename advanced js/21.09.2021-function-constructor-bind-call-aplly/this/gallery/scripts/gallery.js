/*
    Напишите метод который будет применять анимацию fade-in и показывать все изображения из галереи одно за другим с небольшой задержкой после каждого.
    Вызовете метод через 5 секунд.
    Метод перебора для коллекции html элементов позаимствуйте у массива :)

    Пример: https://youtu.be/UDql4w8pExs
 */
//
// const randomGallery = {
//     container: document.querySelector('.gallery'),
//     renderImages(){
//         for (let i = 0; i < 100; i++) {
//             const html = `
//             <div class="gallery__item">
//                 <img src="https://picsum.photos/200/200?v=${i}" />
//             </div>
//             `
//             this.container.insertAdjacentHTML("beforeend", html);
//         }
//     },
//     showImage(image, timeout) {
//         setTimeout(() => {
//             image.classList.add('fade-in');
//             image.style.opacity = 1;
//         }, timeout)
//     },
//
//     showImages(){
//         const imagesCollection = document.getElementsByClassName('gallery__item');
//         console.log(imagesCollection);
//         [].forEach.call(imagesCollection, (e,index) => {
// this.showImage(e,500 + 50 * index);
//         })
//     }
// }
//
// randomGallery.renderImages();
// setTimeout(randomGallery.showImages.bind(randomGallery),5000);





function RandomGallery (selector, numberImage, timeout) {
    this.container = document.querySelector(selector);
    this.numberImage = numberImage;
    this.timeout = timeout;
}
RandomGallery.prototype.renderImages = function () {
    for (let i = 0; i < this.numberImage; i++) {
        const html = `
            <div class="gallery__item">
                <img src="https://picsum.photos/200/200?v=${i}" />
            </div>
            `
        this.container.insertAdjacentHTML("beforeend", html);
    }
}
RandomGallery.prototype.showImage = function (image, timeout) {
    setTimeout(() => {
        image.classList.add('fade-in');
        image.style.opacity = 1;
    }, timeout)
}
RandomGallery.prototype. showImages = function () {
    const imagesCollection = document.getElementsByClassName('gallery__item');
    console.log(imagesCollection);
    [].forEach.call(imagesCollection, (e,index) => {
        this.showImage(e,this.timeout * index);
    })
}
const newGallery = new RandomGallery(".gallery", 25, 2500);


newGallery.renderImages();
newGallery.showImages();



