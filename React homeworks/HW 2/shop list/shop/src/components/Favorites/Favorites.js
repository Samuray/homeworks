import { Component }  from "react";
import PropTypes from "prop-types";
import style from "./Favorites.module.scss";
import { ReactComponent as StarAdd } from "../../assets/svg/star-check-outline.svg";
import { ReactComponent as StarRemove} from "../../assets/svg/star-check.svg";

 class Favorites extends Component {
  render() {
    const { code, favoritesArr, onClickSetFavorites } = this.props;
    const isFavorites = favoritesArr.includes(code);
    return (
      <span className={style.favorite}>
        {isFavorites ? (
          <i
            className={` material-icons`}
            onClick={() => {
              onClickSetFavorites(favoritesArr.filter((n) => n !== code));
            }}
          >
                <StarRemove />
          </i>
        ) : (
          <i
            className={` material-icons`}
            onClick={() => {
              onClickSetFavorites([...favoritesArr, code]);
            }}
          >
                <StarAdd />
          </i>
        )}
      </span>
    );
  }
}
Favorites.propTypes = {
  code: PropTypes.string,
  favoritesArr: PropTypes.array,
  onClickSetFavorites: PropTypes.func.isRequired,
};

Favorites.defaultProps = {
  favoritesArr: [],
};

export default Favorites;




