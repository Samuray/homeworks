import React from 'react';
import Button from "../Button";
import styles from './GoBack.module.scss';
import {useHistory} from "react-router-dom";

const GoBack = () => {
    const history=useHistory();
const goooBack=()=>{
    history.goBack();
}

    if (history.length <= 1){return null}
    return (
        <div className={styles.root}>
            <Button onClick={goooBack}>Go Back</Button>
        </div>
    );
}

export default GoBack;