import React from 'react';
import { Formik, Form as FormikForm, FastField, Field } from "formik";
import CustomInput from "../CustomInput";
import CustomInputForField from "../CustomInputForField";
import * as yup from 'yup';

const Form = () => {

    const initialValues = {
        first: '',
        second: '',
        third: '',
        fourth: '',
        fifth: '',
    }

    const validationSchema = yup.object().shape({
        first: yup.string().required('Обязательное поле'),
        second: yup.string().required('Обязательное поле'),
        third: yup.string().required('Обязательное поле'),
        fourth: yup.string().required('Обязательное поле'),
        fifth: yup.string().required('Обязательное поле'),
    })

    return (
        <Formik
            initialValues={initialValues}
            onSubmit={(values) => console.log(values)}
            validationSchema={validationSchema}
        >
            <FormikForm>
                <FastField name="first" label="first" component={CustomInputForField}/>
                <FastField name="second" label="second" component={CustomInputForField}/>
                <FastField name="third" label="third" component={CustomInputForField}/>
                <FastField name="fourth" label="fourth" component={CustomInputForField}/>
                <FastField name="fifth" label="fifth" component={CustomInputForField}/>

                {/*<CustomInput name="first" label="first" />*/}
                {/*<CustomInput name="second" label="second" />*/}
                {/*<CustomInput name="third" label="third" />*/}
                {/*<CustomInput name="fourth" label="fourth" />*/}
                {/*<CustomInput name="fifth" label="fifth" />*/}

                <button type="submit">Submit</button>
            </FormikForm>
        </Formik>
    )
}

Form.propTypes = {};
Form.defaultProps = {};

export default Form;