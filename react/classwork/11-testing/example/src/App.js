import Header from "./components/Header"
import './App.scss';
import {BrowserRouter} from 'react-router-dom'
import Routes from "./Routes";
import { Provider } from "react-redux";
import store from "./store/appStore";
import { useEffect } from "react";
import instanse from "./api";
import Modal from "./components/Modal";

function App() {


  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className="App">
          <Header/>
          <Routes />
          <Modal />
        </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
