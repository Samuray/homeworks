import React from 'react';
import styles from './GoToTop.module.scss';
import useScrollPosition from "../../hooks/useScrollPosition";

const GoToTop = () => {
    const scrollPosition = useScrollPosition(500);
    const isShow = scrollPosition > 400;

    if (!isShow) return null;

    return (
        <>
            <button onClick={() => {}} className={styles.root}>UP</button>
        </>
    )
}

export default GoToTop;