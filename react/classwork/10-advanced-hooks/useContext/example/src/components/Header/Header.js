import React, {useContext} from 'react';
import PropTypes from "prop-types";
import styles from './Header.module.scss';
import {NavLink} from "react-router-dom";
import AppContext from "../../context/AppContext";

import {ReactComponent as DarkSVG} from "../../assets/dark.svg";
import {ReactComponent as LightSVG} from "../../assets/light.svg";
import classNames from "classnames";

const Header = (props) => {
    const {title} = props;
    const { themeStyles, setIsLight, isLight } = useContext(AppContext);


    return (
        <header className={classNames(styles.root, themeStyles.header)}>
            <span>{title}</span>
            <nav>
                <ul>
                    <li>
                        <NavLink to='/' exact activeClassName={styles.activeLink}>Home</NavLink>
                    </li>
                    <li>
                        <NavLink to='/users' activeClassName={styles.activeLink}>Users</NavLink>
                    </li>
                </ul>
            </nav>

            <button onClick={() => setIsLight(prev => !prev)} className={styles.themeBtn}>{ isLight ? <DarkSVG /> : <LightSVG />}</button>

        </header>
    );
};

Header.propTypes = {
    title: PropTypes.string,
};

Header.defaultProps = {
    title: "Hello",
};

export default Header;
