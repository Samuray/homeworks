import { useEffect, useState } from "react";

const useDeviseType =() => {
    const [type, setType] = useState(null);

    const showWidth = () => {
        if (window.innerWidth <= 576) {
            setType('mobile');
        } else if(window.innerWidth <= 992) {
            setType('tablet');
        } else {
            setType('desktop');
        }
    };

    useEffect(() => {
        window.addEventListener('resize', showWidth);
        showWidth();

        return () => {
            window.removeEventListener('resize', showWidth);
        }
    }, []);

    return type;
};

export default useDeviseType;