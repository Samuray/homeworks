import React from 'react';
import PropTypes from "prop-types";
import styles from './Header.module.scss';
import { useLocation, useHistory } from "react-router-dom";
import HeaderNav from "../HeaderNav/HeaderNav";

const Header= (props) => {
        const { title, user: { name, age, avatar } } = props;
        const location = useLocation();
        const history = useHistory();

    console.log('location FROM HEADER', location);
    console.log('history FROM HEADER', history);

        return (
              <header className={styles.root}>
                  <span>{title}</span>
                    <HeaderNav/>
                  <div className={styles.userContainer}>
                      <img src={avatar} alt={`user ${name}`} />
                      <span>{name}, {age}</span>
                  </div>
              </header>
        );
};

Header.propTypes = {
    title: PropTypes.string ,
    user: PropTypes.shape(
        {
            name: PropTypes.string,
            age: PropTypes.number,
            avatar: PropTypes.string
        }
    )
}

Header.defaultProps = {
    title: "Hello",
    user: {
        name:"Marina",
        age: 25,
        avatar: ""
    }
}

export default Header;
