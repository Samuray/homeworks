import React, {useState, useMemo, useCallback} from 'react';
import Item from "../Item";

const Container = () => {
    const [value, setValue] = useState(true); // Меняет state компонента по клику и вызывает перерисовку

    const [another, setAnother] = useState(true); // Передается как пропс, не меняется
    const callback = useCallback(() => setAnother(prev => !prev), []); // Передается как пропс, не меняется
    const array = useMemo(() => [1,2,3,4,5], []); // Передается как пропс, не меняется


    return (
        <div className="container">
            <h3>CONTAINER</h3>
            <button onClick={() => setValue(prev => !prev)}>Change value</button>
            <Item another={another} />
        </div>
    )
}

export default Container;