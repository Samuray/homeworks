import './App.css';
import React from 'react';
import Modal from "./components/Modal/Modal";

class App extends React.Component {

  state = {
    isOpenFirst: false,
    isOpenSecond: false,
  }

  openModal = () => {
      this.setState({
          isOpen: true,
      })
    }

    closeModal = (number) => {

        this.setState({
            isOpen: false,
        })
    }

  render (){
    return (
        <div className="App">
          <button onClick={(event) => {
              this.setState(current => ({...current, isOpenFirst: !current.isOpenFirst}));
              event.target.blur();
          }}>Open First</button>

            <button onClick={(event) => {
                this.setState(current => ({...current, isOpenSecond: !current.isOpenSecond}));
                event.target.blur();
            }}>Open Second</button>

          {this.state.isOpenFirst && <Modal
              closeModal={this.closeModal}
              backgroundColor="green"
              actions={<><button onClick={this.closeModal}>Hello</button></>}
              text="First text Modal"
          />}

            {this.state.isOpenSecond && <Modal
                closeModal={this.closeModal}
                backgroundColor="green"
                actions={<><button onClick={this.closeModal}>Hello</button></>}
                text="Second text Modal"
            />}
        </div>
    );
  }
}

export default App;
