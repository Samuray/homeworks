import sum from './index';

describe('Sum work', () => {
    test('should function return sum', () => {
        expect(sum(3, 2)).toBe(5);
    });

    test('should sum 3 + 2 not equal 6', () => {
        expect(sum(3, 2)).not.toBe(6);
    });
})
