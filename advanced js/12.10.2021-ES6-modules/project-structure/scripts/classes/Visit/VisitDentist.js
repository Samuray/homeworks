import Visit from "./Visit.js";

class VisitDentist extends Visit {
    constructor(name) {
        super(name);
    }
};

export default VisitDentist;
