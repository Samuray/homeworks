const express = require('express');
const colors = require('colors');
const cors = require('cors');
const bodyParser = require("body-parser");
const cookieParser = require('cookie-parser')
const elems = require('./bdMock/elems');
const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use(cookieParser());

app.get('/', (request, response) => {
    response.status(200);
    const body = `<div style='font-family: sans-serif;'><h1>Hello from server</h1><p>Send <strong>POST</strong> request to <a href="http://localhost:3000/element">http://localhost:3000/element</a> to add new element</p><p>Send <strong>GET</strong> request to <a href="http://localhost:3000/elements">http://localhost:3000/elements</a> to get list of elements</p></div>`
    response.send(body);
})

app.get('/elements', (request, response) => {
    // console.log(request.cookies);
    response.status(200);
    const body = {
        status: 'success',
        data: [...elems],
    }
    response.send(JSON.stringify(body));
})

app.post('/element', (req, res) => {

    if(req.headers && req.headers['content-type']) {
        if (!(/application\/json/gi.test(req.headers['content-type']))){
            return res.status(418).send({
                status: `error`,
                error: `Wrong Content-type header: expected 'application/json' instead of '${req.headers['content-type']}'`
            })
        }
    }

    elems.push(req.body);

    return res.status(200).send(JSON.stringify({
        status: 'success',
        data: [...elems]
    }))
})



app.listen(3000, () => {
    console.log('================================================'.white)
    console.log('Host is listening on PORT: 3000'.white);
    console.log('http://localhost:3000/'.cyan);
    console.log('================================================'.white)
});
