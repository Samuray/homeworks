"use strict"


const btnWrap = document.querySelector('.btn-wrapper'); //обьявляем наш див
document.addEventListener('keyup', highlightLetters);   // вызываем событие клавиатуры с функцией
function highlightLetters(event) {              // функция
    const keyUp = event.key.toUpperCase();
    const key = event.key;
    const className = 'active';     // присваиваем класс актив
    for (let btn of btnWrap.children) { //перебираем детей нашего дива
        if (btn.classList.contains(className)) {
            btn.classList.toggle(className);    //добавляем или удаляем класс актив
        }
        if (btn.innerText === keyUp) {
            btn.classList.toggle(className);
        } else if (btn.innerText === key) {
            btn.classList.toggle(className);        // условия при которых это происходит
        }
    }
}




