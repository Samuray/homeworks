import axios from 'axios';
import { getFromLS } from '../utils/localStorage';

const instanse = axios.create({ 
    baseURL: 'http://localhost:3001/'
 })

instanse.interceptors.request.use((config) => {
    if (getFromLS('token')) {
        config.headers = {
            'Authorization': `Bearer ${getFromLS('token')}`
        }
    }

    return config;
})

 export default instanse;