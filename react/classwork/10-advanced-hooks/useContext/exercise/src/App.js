import React, {useContext, useState} from 'react';
import { BrowserRouter } from 'react-router-dom';
import './App.scss';
import Header from "./components/Header";
import Routes from "./Routes";
import LangContextProvider from "./contex/LangContextProvider";

function App() {
  return (
      <LangContextProvider>
          <BrowserRouter>
            <div className="App">
                <Header title="Context" />
                <Routes />
            </div>
          </BrowserRouter>
      </LangContextProvider>
  );
}

export default App;
