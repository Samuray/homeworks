/*
1. Выведите в параграф с id = href строку url текущей страницы в браузере;
2. Напишите функционал перезагрузки страницы по клику на кнопку с id = reload;
 */

const {location: {href, reload}}= window;
const reload = window.location.reload

console.log(href);
console.log(reload);

const paragraph = document.querySelector("#href");
paragraph.innerHTML = href;

const reloadBtn = document.querySelector("#reload");

reloadBtn.addEventListener("click", reload.bind(window.collection))

