# try...catch
<hr />

<blockquote>
Конструкция try...catch пытается выполнить инструкции в блоке try, и, в случае ошибки, выполняет блок catch.
</blockquote>


## Синтаксис

```js

try {
    // ...код приложения
} catch (error) {
    // ...обработка ошибки. Проигнорируется если ошибок в try не будет.
    console.log(error.name);
    console.log(error.message);
} finally {
    // выполниться в любом случае
}

```
<hr />

## Что за ошибки? 

### [RangeError](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/RangeError)

```js

const toFixed = (number) => {
    return function(numberAfterPoint) {
        return number.toFixed(numberAfterPoint);
    }
}

const fixed = toFixed(2.123123123123)(2);
const fixed1 = toFixed(2.123123123123)(5);
const fixed2 = toFixed(2.123123123123)(Number.MAX_SAFE_INTEGER);


console.log(fixed);
console.log(fixed1);
console.log(fixed2);

```
<hr />


### [ReferenceError](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/ReferenceError)

```js

const x = 23;
const z = 11;

const logThreeVars = () => {
    console.log(x);
    console.log(y);
    console.log(z);
}

logThreeVars();

```
<hr />

### [TypeError](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/TypeError)

```js

const menAndDogs = [
    {
        name: 'Jhon',
        dog: {
            name: 'Lucky'
        },
    },
    {
        name: 'Sam',
    },
    {
        name: 'Bob',
        dog: {
            name: 'Good Boy'
        },
    },
];

const logAllMenAndDogs = (array) => {
    array.forEach(elem => console.log(`${elem.name} and his ${elem.dog.name}`));
}

logAllMenAndDogs(menAndDogs);

```
<hr />


### [SyntaxError](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/SyntaxError)

```js

const jsonString = JSON.stringify(['Hello, friend', {key: 'value'}, [1, 2, 3]]);
const badString = () => {};

const logParsedString = (string) => console.log(JSON.parse(string));


logParsedString(jsonString);
logParsedString(badString);
logParsedString(jsonString);

```
<hr />

### Безусловный блок catch
При использовании блока catch, он вызывается для любого исключения в блоке try.

```js

try {
    // код приложения
}
catch (e) {
   // инструкции для обработки ошибок
}

```
<hr />

### Yсловный блок catch
"Условные блоки catch" можно создавать, используя try...catch с if...else if...else

```js

try {
    // код приложения
} catch (e) {
    if (e instanceof TypeError) {
        // обработка исключения TypeError
    } else if (e instanceof RangeError) {
        // обработка исключения RangeError
    } else if (e instanceof EvalError) {
        // обработка исключения EvalError
    } else {
        // обработка остальных исключений
    }
}
```
<hr />

### [Throw](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/throw)
<hr />

### Проброс исключений

Проброс исключений используется если мы хотим отслеживать лишь определенный их тип, например, SyntaxError, а остальные не трогать – пересылать дальше – блокам try более высокого уровня.

Для его реализации нам достаточно проверить во внутреннем блоке catch тип ошибки и, если она не соответствует SyntaxError, то передать дальше:

```js


const myFunction = () => {
    try {
        // Главный try
        const x = 3;
        const y = 4;
        const z = x + y;

        const doSomethingSmall = () => {
            try {
                // Иногда генерирует неважные для нас RangeError. Было принято решение игнорировать их.
                // Все остальные ошибки при этом необходимо обработать.
                throw new RangeError();
            } catch (error) {
                if (error.name === 'RangeError') { // если RangeError - игнорируем ее
                    console.log('Опять неважная ошибка.');
                } else {
                    throw error; // иначе пробрасываем дальше
                }
            }
        }

        doSomethingSmall();

        console.log(x);
        console.log(y);
        console.log(z);

    } catch (error) {
        // Главный catch умеет работать со всеми ошибками
        alert('Внимание, ошибка в консоли');
        console.error(error);
    }
}

myFunction();


```
<hr />

### [Optional chaining](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Operators/Optional_chaining)
