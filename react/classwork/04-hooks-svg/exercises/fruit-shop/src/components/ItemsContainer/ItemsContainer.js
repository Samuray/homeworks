import React from 'react';
import styles from './ItemsContainer.module.scss';
import Item from "../Item";

const ItemsContainer = (props) => {
    const {items, addItem, toggleFav} = props;

    return (
        <section className={styles.root}>
            <h1>ITEMS</h1>
            <div className={styles.container}>
                {items && items.map(item => <Item toggleFav={toggleFav} key={item.name} {...item} addItem={addItem} />)}
            </div>
        </section>
    )
}

export default ItemsContainer;