import {ADD_ITEM_CART, INIT_ITEM_CART} from "../actions";

const initialState = {
    data:[],
}

const cartReducer = (state = initialState , {type, payload}) => {
    switch (type) {
        case INIT_ITEM_CART: {
            return {...state, data: payload}
        }
        case ADD_ITEM_CART:{
            localStorage.setItem('cart', JSON.stringify([...state.data, payload]))
            return {...state, data: [...state.data, payload]}
        }

        default: { return state}


    }
}

export default cartReducer