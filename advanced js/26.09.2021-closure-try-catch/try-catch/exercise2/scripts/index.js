const colors = [
    '#92a8d1',
    '#deeaee',
    '#b1cbbb',
    '#c94c4c',
    '#f7cac9',
    '#f7786b',
    '#82gdfgb74b',
    '#405d27',
    '#405d271231',
    '#b5e7a0',
    '#eca1a6',
    '#d64161',
    '#b2ad7f',
    '#6b5b95',
];
class InvalidHexError extends Error {
    constructor(hexColor) {
        super();
        this.name = "InvalidHexError";
        this.message = `hex invalid: ${hexColor}`;
    }
}

class HexItem {
    constructor(color) {
     if (color.length !== 7) {
       throw new InvalidHexError(color);
     }
        this.color = color;
    }
    render(selector) {
        document.querySelector(selector).insertAdjacentHTML("beforeend", `
    <div class="item" style="background:${this.color}">
    <span>${this.color}</span>
    </div>`)
    }
}

colors.forEach(e => {
    try {
        new HexItem(e).render(".colors-container");
    }
    catch (error) {
    console.error(error);
    }
});

