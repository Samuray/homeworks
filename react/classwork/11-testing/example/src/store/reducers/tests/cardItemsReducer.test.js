import cardItemsReducer from "../cardItemsReducer";
import {INIT_CARDS} from "../../actions/cardItemsActions";
import {IS_FAVORITE} from '../../actions/cardItemsActions'
import instanse from "../../../api";


const initCardItemsCreator = (data) => {
    return ({type: INIT_CARDS, payload: data})
}
const initialState = {
    cards: [{
        id: 1,
        isFavorite: false
    }],
}

const setIsFavoriteCreator = (item) => ({type: IS_FAVORITE, payload: item})


describe('Reducer init state', () => {
    test('should state init', () => {
        expect(cardItemsReducer()).toStrictEqual({
            cards: [],
        })
    })
})
describe('Reducer actions work ', () => {
    test('should return proper cards with INIT_CARDS type', () => {
        expect(cardItemsReducer(initialState, initCardItemsCreator(5))).toStrictEqual({cards: 5})
    });

    test('should return proper cards with IS_FAVORITE type', () => {
        expect(cardItemsReducer(initialState, setIsFavoriteCreator({
            id: 1,
            isFavorite: true
        }))).toStrictEqual({
            cards: [{
                id: 1,
                isFavorite: true
            }]
        })
    })
})