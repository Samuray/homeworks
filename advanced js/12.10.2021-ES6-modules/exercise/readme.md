# Search and filter

1. Получите данные по `https://ajax.test-danit.com/api/json/posts`
2. Выведите все карточки на экран
3. Реализуйте функционал фильтрации постов через input и select

P.S. Используйте модули, классы, и best practises
<hr />

Шаблон карточки:
```html

<div class="card">
    <h3>${this.title}</h3>
    <p>${this.text}</p>
</div>

```
