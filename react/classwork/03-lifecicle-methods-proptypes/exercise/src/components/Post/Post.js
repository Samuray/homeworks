import React, { PureComponent } from 'react';
import styles from './PostsContainer.module.scss';
import PropTypes from 'prop-types';
import Header from "../Header";

class Post extends PureComponent {
    render(){
        const { userId, title, body } = this.props;

        return (
            <div className={styles.root}>
                <span>User: {userId}</span>
                <h3>{title}</h3>
                <p>{body}</p>
            </div>
        );
    }
};

Post.propTypes = {
    userId: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    title: PropTypes.string.isRequired,
    body: PropTypes.string.isRequired,
}

Post.defaultProps = {
    userId: 1,
}

export default Post;
