### Анимация с setInterval/setTimeout

Дан объект 
```javascript
const autoSlider = {
    images: [
    'images/1.jpg',
    'images/2.jpg',
    'images/3.jpg',
    'images/4.jpg',
    'images/5.jpg',
    ],
    titles: [
    'The greatest glory',
    'Your time is limited',
    'If life were predictable',
    'If you set your goals',
    'Life is what happens',
    ],
    currentSlide: 0,
    container: document.querySelector('.container'),
    imageElem: document.querySelector('.image-container img'),
    textElem: document.querySelector('.text-container'),
    
    start (){},
};
```

Реализуйте автоматическую смену изображений и текста на экране с анимацией fade-out/fade-in. Используйте:
```css
@keyframes fade-in {
    from {
        opacity: 0;
    }

    to {
        opacity: 1;
    }
}

@keyframes fade-out {
    from {
        opacity: 1;
    }

    to {
        opacity: 0;
    }
}

.fade-in {
    animation: fade-in 1.5s ease;
}

.fade-out {
    animation: fade-out 1.5s ease;
}
```
