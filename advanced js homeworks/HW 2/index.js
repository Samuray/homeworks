


const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];



let root = document.getElementById("root");
let ul = document.createElement("ul");
root.appendChild(ul);

books.forEach(item => {
    try {
        if (item.author !== undefined && item.name !== undefined && item.price !== undefined){
            ul.insertAdjacentHTML('beforeend', `
    <li>
         <span>Author:${item.author} </span>        
         <span>Name:${item.name}</span>
         <span>Price:${item.price}</span>
    </li> `)
        } else {
            throw item;
        }
    } catch (error) {
        console.error(error);
    }
});




