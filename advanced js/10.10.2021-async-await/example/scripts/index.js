const resolveAfterSomeSeconds = (value, timeot) => new Promise(resolve => {
    setTimeout(() => {
        resolve(value);
    }, timeot);
});

const getValueSum = async () => {
    const [a, b] = await Promise.all([resolveAfterSomeSeconds(20, 2000), resolveAfterSomeSeconds(30, 3000)])
    const sum = a + b;
    console.log(sum);
}

getValueSum();
