import React, {useState, useRef} from 'react';
import classNames from 'classnames';
import styles from './App.module.scss';

function App() {
    const [isOpen, setIsOpen] = useState(false);
    const inputRef = useRef(null)

    return (
        <div className={styles.App}>
            <div className={styles.header}>
                <button onClick={() => {
                    setIsOpen(prev => !prev);
                        setTimeout(() => {
                            console.log(isOpen)
                            if(!isOpen) {
                                inputRef.current.focus();
                            }
                        }, 300);
                }}>Search</button>
            </div>

            <div className={classNames(styles.searchContainer, { [styles.open]: isOpen })}>
                <input ref={inputRef} type="search" name="search"/>
                <button>OK</button>
            </div>
        </div>
    );
}

export default App;
