import React from "react";

const HomePage = () => {

    return (
        <section>
            <h1 >HOME</h1>
            <h2>Awesome home page is here</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid amet asperiores at atque doloribus est, fugiat iusto minus molestiae obcaecati odit porro praesentium quasi rem vitae voluptates voluptatum. At earum labore pariatur provident tempore voluptatum?</p>
        </section>
    )
}

export default HomePage;