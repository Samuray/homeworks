"use strict";

if (!localStorage.theme) localStorage.theme = "light"
document.body.className = localStorage.theme
btn.innerText = document.body.classList.contains("dark") ? "Light" : "Dark"

btn.onclick = () => {
document.body.classList.toggle("dark")
btn.innerText = document.body.classList.contains("dark") ? "Light" : "Dark"
localStorage.theme = document.body.className || "light"
}

