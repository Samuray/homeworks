import React from 'react';
import AppContext from "./AppContext";

const AppContextProvider = (props) => {
    const { value, children } = props;

    return (
        <AppContext.Provider value={value}>
            {children}
        </AppContext.Provider>
    )
}

AppContextProvider.propTypes = {};
AppContextProvider.defaultProps = {};

export default AppContextProvider;