import {toDoReducer} from "./toDoReducer";
import {combineReducers} from "redux";
import {modalReducer} from "./modalReducer";

export const appReducer = combineReducers({
    toDo: toDoReducer,
    modal: modalReducer,
})
