import React, {useContext, useState} from 'react';
import LangContext from "../../contex/LangContext";

const LangSelect= () => {

        const { value, setValue } = useContext(LangContext);

        return (
              <div>
                  <select
                      name="lang"
                      id="lang"
                      value={value}
                      onChange={({ target: { value } }) => setValue(value)}
                  >
                      <option value='EN'>EN</option>
                      <option value='DE'>DE</option>
                      <option value='FR'>FR</option>
                      <option value='UA'>UA</option>
                  </select>
              </div>
        );
};

export default LangSelect;
