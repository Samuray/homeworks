/*
Напишите функцию changeTextSize, у которой будут такие аргументы:
1. Ссылка на DOM-элемент, размер текста которого нужно изменить без регистрации и sms.
2. Величина в px, на которую нужно изменить текст,  возвращает функцию, меняющую размер текста на заданную величину.

С помощью этой функции создайте две:
- одна увеличивает текст на 2px от изначального;
- вторая - уменьшает на 3px.

После чего повесьте полученные функции в качестве  обработчиков на кнопки с id="increase-text" и id="decrease-text".

 */

    const changeTextSize = function (elem, fontStep) {
    const fontSize = elem.style.fontSize
        let  editedFontSize = parseInt(fontSize);
        return function () {
    editedFontSize += fontStep;
    elem.style.fontSize = `${editedFontSize}px`
        }
}
     const changeFontInc2 = changeTextSize(document.querySelector("#paragraph"), 2);
    const changeFontInc3 = changeTextSize(document.querySelector("#paragraph"), -3);
    const incButton = document.querySelector("#increase-text");
    const decButton = document.querySelector("#decrease-text");
    incButton.addEventListener("click", changeFontInc2);
    decButton.addEventListener("click", changeFontInc3);