import removeLetter from "./removeLetter";

describe("remove letters work", () => {
    test("should function remove letters", () => {
        expect(removeLetter("some string_003")).toBe(" _003");
    })
    test("should function remove string without letters", () => {
        expect(removeLetter("_003")).toBe("_003");
    })
})

describe("work with invalid arguments", () => {
    test("should function take number and remove null", () => {
        expect(removeLetter(33)).toBe(null);
    })
})