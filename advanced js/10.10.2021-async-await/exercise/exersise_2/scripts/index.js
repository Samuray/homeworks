const options = {
    method: 'GET',
    url: 'https://fitness-calculator.p.rapidapi.com/mets',
    headers: {
        'x-rapidapi-host': 'fitness-calculator.p.rapidapi.com',
        'x-rapidapi-key': 'ab0637da31msh0652f644f41a177p1a4e5cjsn0c04c39f4fb8'
    }
};

axios.request(options).then(function (response) {
    console.log(response.data);
}).catch(function (error) {
    console.error(error);
});

const getActivities = async ()=> {
    try {
        const selectActivity = document.querySelector('#activity')
        const { data } = await axios.request(options)
        console.log(Object.keys(data))
        const newArr = Object.keys(data).forEach((key) => {
            console.log(data[key]);
            Object.keys(data[key]).forEach(activityKey => {
                console.log(data[key][activityKey])
                selectActivity.insertAdjacentHTML("beforeend", `<option value="${data[key]?.[activityKey]?.MET}" selected>${data[key]?.[activityKey]?.specific}</option>`)
            })
        })
        console.log(newArr);
    } catch (e) {
        console.log(e)
    }
}

getActivities()

const getInputValue = (id) => document.querySelector(`#${id}`).value;


document.querySelector('.button-get-activity').addEventListener('click', () => {
    const weight = getInputValue('weight');
    const activityMET = getInputValue('activity');
    const time = getInputValue('time');

    alert(weight * time * activityMET)
    console.log(weight)
    console.log(activityMET)
    console.log(time)
});


// const options = {
//     method: 'GET',
//     url: 'https://fitness-calculator.p.rapidapi.com/idealweight',
//     params: {gender: 'male', weight: '70', height: '178'},
//     headers: {
//         'x-rapidapi-host': 'fitness-calculator.p.rapidapi.com',
//         'x-rapidapi-key': 'ab0637da31msh0652f644f41a177p1a4e5cjsn0c04c39f4fb8'
//     }
// };
//
// axios.request(options).then(function (response) {
//     console.log(response.data);
// }).catch(function (error) {
//     console.error(error);
// });
