import { Component } from "react";
import style from "./Navbar.module.scss"

 class Navbar extends Component {
  render() {
    return (
      <nav>
        <div className="nav-wrapper  blue darken-3">
          <div className="container">
            <a
              href="#!"
              className="brand-logo"
              style={{ color: "white", fontSize: 40, fontWeight: "bold" }}
            >
              {/*<i*/}
              {/*  className="material-icons"*/}
              {/*  style={{ color: "#ff9100", fontSize: 45 }}*/}
              {/*>*/}

              {/*</i>*/}
              Shop of Phones
            </a>
            <ul className="right hide-on-med-and-down">
              <li>
                <a href="#!">
                  <i className="material-icons left"> </i>
                  Home
                </a>
              </li>
              <li>
                <a href="#!">
                  <i className="material-icons left"> </i>
                  Favorites
                </a>
              </li>
              <li>
                <a href="#!">
                  <i className="material-icons left"> </i>
                  Cart
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default Navbar;



