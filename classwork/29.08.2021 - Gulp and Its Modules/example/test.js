const gulp = require ("gulp");
const concat = require ("gulp-concat");
const src = require.src
const dest = require.dest
const htmlmin = require("gulp-htmlmin");
const terser = require('gulp-terser');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const imagemin = require(("gulp=imagemin"));
 import BS from "browser-sync";
const browserSync =

gulp.task('scripts', function() {
    return gulp.src('src/css/*.css')
        .pipe(concat('all.css'))
        .pipe(gulp.dest('./dist/css'));
});
const watcher = gulp.watch("src/css/*.css");
watcher.on("change", concat)
const buildCssFunc= () => gulp.src("src/css/*.css")
    .pipe(concat("all.css"))
    .pipe(autoprefixer({
        cascade: false
    }))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest("dest/css"));

const cleanDist = () => gulp.src("dist/*", {read: false}) .pipe(clean());
const buildJsFunc = () => gulp.src("src/js/*js").pipe(concat("all.css")).pipe(terser()).pipe(gulp.dest("dist/css"));
const minifyHtmlMin = () => gulp.src("src/css/*.css").pipe(concat("all.js"))
const minifyImages = () => gulp.src("src/images/*") .pipe(imagemin()).pipe(gulp.dest('dist/images'));

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
});


gulp.task ( "buildCss", buildCssFunc());
gulp.task ("buildJs", buildJsFunc);
gulp.task("cleanDst",cleanDist);
gulp.task("minifyImages", minifyImages)