import React from "react";
import styles from "./HeaderNav.module.scss"
import {Link, NavLink} from "react-router-dom";


const HeaderNav = () => {
    return(
        <nav>
            <ul>
                <li>
                    <NavLink to="/sign" activeClassName={styles.activeLink}>Sign in</NavLink>
                </li>
                <li>
                    <NavLink exact to="/home" activeClassName={styles.activeLink}>Home</NavLink>
                </li>
                <li>
                    <NavLink to="/posts" activeClassName={styles.activeLink}>Blog</NavLink>
                </li>
                <li>
                    <NavLink to="/users" activeClassName={styles.activeLink}>Users</NavLink>
                </li>
            </ul>
        </nav>
    )
}

export default HeaderNav