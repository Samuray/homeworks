const express = require('express');
const colors = require('colors');
const cors = require('cors');
const cookieParser = require('cookie-parser')
const { v4: uuidv4 } = require('uuid');
const app = express();

app.use(cors());
app.use(cookieParser());

app.get('/', (request, response) => {
        // console.log(request.cookies);
        response.status(200);
        const body = {
            token: uuidv4(),
            status: 'success'
        }
        response.send(JSON.stringify(body));
})

app.listen(3000, () => {
    console.log('================================================'.white)
    console.log('Host is listening on PORT: 3000'.white);
    console.log('http://localhost:3000/'.cyan);
    console.log('================================================'.white)
});
