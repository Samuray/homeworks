import React, {useState} from 'react';
import styles from './AddNote.module.scss';
import {Button, TextField} from "@mui/material";
import {useDispatch} from "react-redux";
import {addNote} from "../../appStore/actionCreator/actionCreator";

const AddNote = () => {
    const [value, setValue] = useState('');
    const dispatch = useDispatch()
    return (
        <>
            <div className={styles.root}>
                <TextField
                    type='text'
                    label='Your note'
                    value={value}
                    onChange={
                        ({target})=>{
                            setValue(target.value)
                        }
                    }
                    className={styles.input}
                />
                <Button className={styles.btn} variant='contained' onClick={()=>{
                    dispatch(addNote(value))
                    setValue('')
                }}>Add Note</Button>
            </div>
            <div className={styles.line} />
        </>
)
}

export default AddNote;