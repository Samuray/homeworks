
import React from 'react';
import styles from './Modal.module.scss'

export default class Modal extends React.Component {
    render() {
        return (
            <div onClick={this.props.isOpen}
                className={styles.modal}>
                <div onClick={event => event.stopPropagation()}
                     className={styles.modalContent}>
                    <div className={styles.modalHeader}>
                        <h5>{this.props.header}</h5>
                        {this.props.closeButton && <span onClick={this.props.isOpen}>&#10761;</span>}
                    </div>
                    <div className={styles.modalBody}>
                        <p className={styles.bodyText}>{this.props.textmodal}</p>
                    </div>
                    {this.props.actions}
                </div>
            </div>
        )
    }
}







