import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './App.scss';
import ErrorBoundary from "./components/Error/ErrorBoundary";

ReactDOM.render(
    <App />,
  document.getElementById('root')
);

