import React, {useEffect, useState} from 'react';
import PropTypes from "prop-types";

const Title = ({ title, className }) => {
    // const [isParagraph, setIsParagraph] = useState(false)
    //
    // useEffect(() => {
    //     setTimeout(() => setIsParagraph(true), 400)
    // }, []);

    return (
        <>
           <h1 className={className}>{title}</h1>
            {/*{isParagraph && <p>paragraph</p>}*/}
            {/*<button onClick={() => setIsParagraph(prev => !prev)}>Toggle paragraph</button>*/}
        </>
    )
}

Title.propTypes = {
    title: PropTypes.string,
    className: PropTypes.string
};

Title.defaultProps = {
    title: 'Hello',
    className: ''
};

export default Title;