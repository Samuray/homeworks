

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }


    get name() {
        return this._name;
    }
    get age() {
        return this._age;
    }
    get salary() {
        return this._salary;
    }
    set name(value) {
        this._name = value;
    }
    set age(value) {
        this._age = value;
    }
    set salary(value) {
        this._salary = value;
    }


logInfo () {
    console.log(`Name: ${this.name}; Age: ${this.age}; Salary: ${this.salary}`)
        };
    }

const human = new Employee("Boris",30,5000);
human.logInfo();


class  Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
       return this._salary;
    }
    set salary(value) {
        this._salary = value * 3;
    }

    logInfo() {
        super.logInfo()
        console.log(`Lang: ${this.lang}`)
    }
}
const humans = new Programmer("Volodya", 40, 10000, "ukr");
const humansOne = new Programmer("Karl", 35, 30000, "eng");
const humansTwo = new Programmer("Grigoriy", 27, 7000, "rus");
humans.logInfo();
humansOne.logInfo();
humansTwo.logInfo();



