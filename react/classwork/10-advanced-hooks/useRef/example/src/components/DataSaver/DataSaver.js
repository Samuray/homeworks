import React, {useEffect, useState, useRef} from 'react';
import PropTypes from "prop-types";

const DataSaver = ({ value }) => {
    const [fact, setFact] = useState('');
    const ref = useRef(null);
    console.log(ref.current);

    useEffect(() => {
        (async () => {
            if (ref.current) {
                setFact(ref.current);
                return;
            }

            console.log('LOADING STARTED');
            const { fact } = await fetch('https://catfact.ninja/fact').then(res => res.json());
            setFact(fact);
            ref.current = fact;
            console.log('LOADING FINISHED');
        })()

    }, [value]);

    return <p>{fact}</p>;
}

DataSaver.propTypes = {
    value: PropTypes.bool.isRequired,
}


export default DataSaver;