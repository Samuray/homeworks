import React from 'react';
import styles from './Modal.module.scss';

class Modal extends React.PureComponent {
    render(){
        const { closeModal, backgroundColor, actions, text } = this.props;

        return (
            <div className={styles.background} onClick={closeModal}>
                <div className={styles.modal} style={{ backgroundColor }} >
                    <div className={styles.header}>sfddsdfsdf</div>
                    <div className={styles.content}>
                        <p>{text}</p>
                    </div>
                    <div className={styles.buttons}>{actions}</div>
                </div>
            </div>
        );
    }
};

Modal.propTypes = {};

Modal.defaultProps = {};

export default Modal;
