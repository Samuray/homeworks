

const promise = fetch('http://localhost:3000/').then( res => res.json());
const race = new Promise( res => {
    setTimeout( () => {
        res('Long response');
    }, 5000);
} )
const container = document.querySelector('.container');
const preloader = document.querySelector('.preloader');

Promise.race([promise, race]).then( (value) => {
        if (value === 'Long response') {
            document.querySelector('.something-goes-wrong').style.display = 'flex';
            document.querySelector('#reload').addEventListener('click', () => {
                location.reload();
            })
            preloader.style.display = 'none';
            return
        }
        const filteredArray = value.data.filter(({userId: id}) => value.userId === id)
        filteredArray.forEach( ({title, body, userId}) => {
                container.insertAdjacentHTML('beforeend', `<div class="card">
        <span>User ID: ${userId}</span>
        <h2>${title}</h2>
        <p>${body}</p>
        <div>`
                )
            }
        );
        preloader.style.display = 'none';
    }
)
