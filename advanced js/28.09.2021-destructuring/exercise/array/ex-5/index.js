/*
При клике на кнопку с классом button выведите в p с id user-number номер элемента в списке меню, на который мы кликнули.
Используйте полученные знания :)
 */

document.querySelector(".menu").addEventListener("click", ({target, currentTarget:{children}}) => {
    const newArr = [...children];

  const index = newArr.findIndex(elem => elem === target);

  document.querySelector("#user-number").innerHTML = index.toString();
})
