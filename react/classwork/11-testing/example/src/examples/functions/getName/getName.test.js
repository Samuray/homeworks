import getName from './index';

describe("getName works", () => {
    test("should function return name without args", () => {
        expect(getName()).toBe("John");
    })

    test("should nameChanger work", () => {
        const nameChanger = (name) => name.toUpperCase();

        expect(getName(nameChanger)).toBe("JOHN");
    })

    test("should nameChanger work", () => {
        const additionalCallback = jest.fn();

        getName(null, additionalCallback);
        getName(null, additionalCallback);

        expect(additionalCallback).toBeCalledTimes(2);
    })

})
