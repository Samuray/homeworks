# Деструктурирующее присваивание

<blockquote>
Деструктурирующее присваивание – это специальный синтаксис, который позволяет нам «распаковать» массивы или объекты в кучу переменных, так как иногда они более удобны. Деструктуризация также прекрасно работает со сложными функциями, которые имеют много параметров, значений по умолчанию и так далее.
</blockquote>

### «Деструктуризация» не означает «разрушение».
«Деструктурирующее присваивание» не уничтожает массив или объект. Оно вообще ничего не делает с правой частью присваивания, его задача – только скопировать нужные значения в переменные.
<hr>

# Деструктуризация массивов

```js

const foo = ["one", "two", "three"];

// без деструктурирования
const one = foo[0];
const two = foo[1];
const three = foo[2];

// с деструктурированием
const [one, two, three] = foo;

```

## Обмен значений переменных

```js

let a = 1;
let b = 3;

[a, b] = [b, a];

console.log(a);
console.log(b);

```

## Возврат нескольких значений

```js

const f = () => [1, 2];

const [a, b] = f();

console.log('a: ', a);
console.log('b: ', b);

```

## Игнорирование некоторых значений

```js

const f = () => [1, 2, 3];

const [a, , b] = f();

console.log('a: ', a);
console.log('b: ', b);

```
<hr>

# Деструктуризация объектов

```js

const object = {
    p: 42, 
    q: true,
};

// без деструктурирования
const p = object.p;
const q = object.q;

// c деструктурирования
const {o, p} = object;

// Объявление новых переменных
const {p: foo, q: bar} = object;

```

## Вложенный объект и разбор массива

```js

const metadata = {
    title: "Scratchpad",
    translations: [
       {
        locale: "de",
        localization_tags: [ ],
        last_edit: "2014-04-14T08:43:37",
        url: "/de/docs/Tools/Scratchpad",
        title: "JavaScript-Umgebung"
       }
    ],
    url: "/en-US/docs/Tools/Scratchpad"
};

const { title: englishTitle, translations: [{ title: localeTitle }] } = metadata;

```

```js
// Получите из объекта id, title, _meta.data[0].id как metaId, _meta.data[0].title как metaTitle,
const posts = {
    _meta: {
        url: 'https://some-posts-ulr.com',
        data: [
            {
                id: '8b12d4f94-1a554ce-428e6-be19-10123f2d0ad36',
                title: 'destructuring-assignment-post'
            }
        ]
    },
    id: 32,
    title: 'Destructuring assignment',
    text: 'The destructuring assignment syntax is a JavaScript expression that makes it possible to unpack values from arrays, or properties from objects, into distinct variables.'
}

const {_meta: { data: [{ id: metaId, title: metaTitle }]}, title, id} = posts;

```


## Деструктурирование во время обхода

```js

const people = [
  {
    name: "Mike Smith",
    family: {
      mother: "Jane Smith",
      father: "Harry Smith",
      sister: "Samantha Smith"
    },
    age: 35
  },
  {
    name: "Tom Jones",
    family: {
      mother: "Norah Jones",
      father: "Richard Jones",
      brother: "Howard Jones"
    },
    age: 25
  }
];

people.forEach(({ name: n, family: { father: f } }) => console.log("Name: " + n + ", Father: " + f));

```

## Получение полей объекта-параметра функции

```js

const whoIsUser = ({displayName, fullName: {firstName: name}}) => {
  console.log(displayName + " is " + name);
}

const user = {
  id: 42,
  displayName: "jdoe",
  fullName: {
      firstName: "John",
      lastName: "Doe"
  }
};

whoIsUser(user);

```

## Остаточные параметры (rest parameters)

```js

const logArgs = function(...args) {
  conole.log(args);
};

logArgs(1, 2, 3, 4, 5); // [1, 2, 3, 4, 5]

```

```js

const array = [1, 2, 3, 4, 5, 6, 7];
const [a, b, ...args] = array;

console.log(a); // 1
console.log(b); // 2
console.log(args); // [3, 4, 5, 6, 7];

```

```js

const object = {
    name: 'John',
    lastName: 'Johnson',
    age: 24,
    city: 'NYC'
};

const { name, age, ...other } = object;

console.log(name); // John
console.log(age); // 24
console.log(other); // { lastName: 'Johnson', city: 'NYC'};

```

## Оператор spread

```js

const logArgs = function(a, b, c) {
    console.log(a);
    console.log(b);
    console.log(c);
};

const array = ['Spread', 'Rest', 'Operator']

logArgs(...array);


```

```js

logArgs.apply(null, [1, 2, 3]); // 1 2 3
// Равнозначно
logArgs(...[1, 2, 3]); // 1 2 3

```

### Клонирование свойств массивов/объектов

```js

const arr = ['will', 'love'];
const data = ['You', ...arr, 'spread', 'operator'];
console.log(data); // ['You', 'will', 'love', 'spread', 'operator']

```

```js

const props = {
    a: 1,
    b: 2,
    c: 3,
}

const object = {
    name: 'John',
    lastName: 'Johnson',
    ...props,
}

console.log(object)

```

### Копирование массива/объекта

```js

const array = ['Spread', 'Rest', 'Operator', [1, 2, 3]];

const newArray = [...array];

```

```js

const object = {
    name: 'John',
    lastName: 'Johnson',
    props: {
        a: 1,
        b: 2,
    },
}

const newObject = {...object};

```

### Преобразование коллекции DOM элементов

```js

const linksCollection = document.getElementsByTagName('a');
const linksArray = [...linksCollection];

console.log(linksCollection);
console.log(linksArray);

```
