class ToDoItem {
    constructor(text, id) {
        this.text = text;
        this.id = id;
        this.container = document.createElement('div');
        this.deleteButton = document.createElement('button');
    }

    createElements() {
        this.container.className = 'todo';
        this.container.innerHTML = `<span>${this.text}</span>`;

        this.deleteButton.innerHTML = 'Delete';
        this.deleteButton.addEventListener("click", async () => {
            try {
                const myQuery = await axios.delete(`http://localhost:3000/to-do?toDoId=${this.id}&userId=${JSON.parse(localStorage.getItem("user")).id}`, {
                    headers: {
                        "Authorization": `Token ${localStorage.getItem('authToken')}`
                    }
                })
            } catch (error) {
                console.log(error);
            }

            localStorage.setItem('user', JSON.stringify(myQuery.data.user));
            toDoRender();

        })
        this.container.append(this.deleteButton);
    };

    render(selector){
        this.createElements();
        document.querySelector(selector).append(this.container);
    }
}

const toDoRender = () => {
    const updatedUser = JSON.parse(localStorage.getItem('user'));
    document.querySelector('.todo-container').innerHTML = '';
    updatedUser.toDoList.forEach( ({text, id}) => {
        new ToDoItem(text, id).render('.todo-container');
    } )
    console.log(updatedUser)
}

const showLogOut = () => {
    const header = document.querySelector('.header');
    header.querySelectorAll('a').forEach(link => link.style.display = 'none');

    const logOutButton = document.querySelector('.log-out');
    logOutButton.style.display = 'inline';

    logOutButton.addEventListener("click", () =>{
        localStorage.removeItem("user");
        localStorage.removeItem("authToken");
        location.reload();
    })
}

const getUser = () => {
    if(localStorage.getItem("user")){
        const {name, age, avatar, city} = JSON.parse(localStorage.getItem("user"));
        document.querySelector(".user-container").insertAdjacentHTML("beforeend", `
        <img src="${avatar}" alt="${name}">
    <p>${name}</p>
    <span>Age: ${age}</span>
    <span>City: ${city}</span>
    <form class="new-todo">
        <input type="text" name="todo" id="todo" placeholder="Add todo">
        <button type="submit">Add</button>
    </form>`)
        showLogOut();
        toDoRender();
    } else{
        document.querySelector('.please-sign-in').style.display = 'block';
    }


}

getUser();

const toDoForm = document.querySelector('.new-todo');



toDoForm.addEventListener('submit', async event => {
    event.preventDefault();
    const body = {
        userId: JSON.parse(localStorage.getItem('user')).id,
        toDo: toDoForm.querySelector('input').value,
    }
    const res = await axios.post('http://localhost:3000/to-do', body, {
        headers : {
            "Authorization": `Token ${localStorage.getItem('authToken')}`
        }
    });
    localStorage.setItem('user', JSON.stringify(res.data.user));
    toDoRender();
    toDoForm.querySelector('input').value = ''
})
