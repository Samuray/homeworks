const url = 'https://ajax.test-danit.com/api/json/photos';

fetch(url)
    .then( (response) => {
        console.log(response)
if (response.ok) {
return response.json()

}else {
    console.error("Bad response!")
}
    })
.then((result) => {
    console.log(result)
result.forEach(({albumId, title, url}) => {
const container = document.querySelector(`#container_${albumId}`);
if(container){container.insertAdjacentHTML("beforeend",
    `<div class="card">
    <img src="${url}" alt="img">
    <span>${title}</span>
            </div>`)}

    })
})