import {Modal} from "./classes/Modal.js";
import { Form } from "./classes/Form.js";
import {Paragraph, Image} from "./classes/NodeElement.js";


const element = document.createElement("div");
element.innerHTML = `<h1>jsgdgsjdfmsbdfmsdmfsmdfsdf</h1>`;
const form = new Form();

const onChooseElem = () =>{
    const selectedValue = form.getSelectValue();
    const inputsValues = form.getInputsValues();
    console.log(selectedValue);
    console.log(inputsValues);

    if (selectedValue === "paragraph") {
        const elem = new Paragraph(inputsValues).render();
        console.log(elem)
        document.querySelector(".container").append(elem);
    }
    if (selectedValue === "image") {
        const elem = new Image(inputsValues).render();
        console.log(elem)
        document.querySelector(".container").append(elem);
    }
};

new Modal(form.render(), onChooseElem).render();

