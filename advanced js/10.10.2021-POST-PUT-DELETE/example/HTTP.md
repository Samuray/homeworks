# [HTTP](https://developer.mozilla.org/ru/docs/Web/HTTP)

<blockquote>
Протокол передачи гипертекста (Hypertext Transfer Protocol - HTTP) - это прикладной протокол для передачи гипертекстовых документов, таких как HTML. Он создан для связи между веб-браузерами и веб-серверами, хотя в принципе HTTP может использоваться и для других целей.
<br />
<br />
Протокол следует классической клиент-серверной модели, когда клиент открывает соединение для создания запроса, а затем ждёт ответа. HTTP - это протокол без сохранения состояния, то есть сервер не сохраняет никаких данных (состояние) между двумя парами "запрос-ответ". 
</blockquote>

### [TCP](https://developer.mozilla.org/ru/docs/Glossary/TCP)
### [Обзор](https://developer.mozilla.org/ru/docs/Web/HTTP/Overview)
### [Кеширование](https://developer.mozilla.org/ru/docs/Web/HTTP/Caching)
### [Cookies](https://developer.mozilla.org/ru/docs/Web/HTTP/Cookies)
### [CORS](https://developer.mozilla.org/ru/docs/Web/HTTP/CORS)
