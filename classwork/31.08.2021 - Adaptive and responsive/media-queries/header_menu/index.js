const button = document.querySelector('.header__menu-button');
const menu = document.querySelector('.header__mobile-menu');

button.addEventListener('click', () => {
    menu.classList.toggle('header__mobile-menu--showed');
})
