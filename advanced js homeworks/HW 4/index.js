
const movie = "https://ajax.test-danit.com/api/swapi/films";

axios.get(movie).then(({ status, data }) => {
    const container = document.querySelector(".container");
    if (status === 200) {
        console.log(data)
        data.forEach(({episodeId, name, openingCrawl, characters, id}) => {
            container.insertAdjacentHTML("beforeend",
                `<div class="card">

                    <p>Name: ${name}</p>
                    <ul id="vehicles-${id}"></ul>
                    <p>Episod id: ${episodeId}</p>
                    <p>Opening crawl: ${openingCrawl}</p>
                      
                    </div>`)
            characters.forEach(charactersUrl => {
                axios.get(charactersUrl).then(({status, data: {name}}) => {
                    if (status === 200) {
                        document.querySelector(`#vehicles-${id}`)
                            .insertAdjacentHTML("beforeend", `<li>${name}</li>`)
                    }
                }).catch(error => console.log(error))
            })
        })
    }
}).catch(error => console.log(error));
