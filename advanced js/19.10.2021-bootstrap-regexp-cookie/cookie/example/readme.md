# [Cookie](https://learn.javascript.ru/cookie)

Куки – это небольшие строки данных, которые хранятся непосредственно в браузере. Они являются частью HTTP-протокола.

Куки обычно устанавливаются веб-сервером при помощи заголовка `Set-Cookie`. Затем браузер будет автоматически добавлять их в (почти) каждый запрос на тот же домен при помощи заголовка `Cookie`.

Один из наиболее частых случаев использования куки – это аутентификация:

1. При входе на сайт сервер отсылает в ответ HTTP-заголовок `Set-Cookie` для того, чтобы установить куки со специальным уникальным идентификатором сессии («session identifier»).
2. Во время следующего запроса к этому же домену браузер посылает на сервер HTTP-заголовок `Cookie`.
3. Таким образом, сервер понимает, кто сделал запрос.

Мы также можем получить доступ к куки непосредственно из браузера, используя свойство `document.cookie`.

## Чтение из `document.cookie`

```js

console.log(document.cookie);

```

## Запись в `document.cookie`

<blockquote>
Запись в document.cookie обновит только упомянутые в ней куки, но при этом не затронет все остальные.
</blockquote>

```js

document.cookie = 'name=John';
document.cookie = 'age=32';
console.log(document.cookie);

```

## encodeURIComponent

Технически, и имя и значение куки могут состоять из любых символов, для правильного форматирования следует использовать встроенную функцию encodeURIComponent:

```js

// специальные символы (пробелы), требуется кодирование
let name = "my name";
let value = "John Smith"

// кодирует в my%20name=John%20Smith
document.cookie = encodeURIComponent(name) + '=' + encodeURIComponent(value);

console.log(document.cookie); // ...; my%20name=John%20Smith

```

# Настройки

У куки есть ряд настроек, многие из которых важны и должны быть установлены.

Эти настройки указываются после пары ключ=значение и отделены друг от друга разделителем `;`, вот так:

```js

document.cookie = "user=John; path=/; expires=Tue, 19 Jan 2038 03:14:07 GMT"

```

### [path](https://learn.javascript.ru/cookie#path)

### [domain](https://learn.javascript.ru/cookie#domain)

### [expires, max-age](https://learn.javascript.ru/cookie#expires-max-age)

### [secure](https://learn.javascript.ru/cookie#secure)

### [httpOnly](https://learn.javascript.ru/cookie#httponly)

## [Готовые функции для работы с cookie](https://learn.javascript.ru/cookie#prilozhenie-funktsii-dlya-raboty-s-kuki)
