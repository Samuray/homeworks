/**
 * @param a {number}
 * @param b {number}
 * @returns {number}
 */
const sum = (a, b) => {
    return a + b;
}

export default sum;