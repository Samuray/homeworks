export const nameValidation = ({ target }) => {
    const regExp = /[^a-zA-Z\s]/g;
    const newValue = target.value.replace(regExp, '');
    target.value = newValue;
}

export const commentValidation = ({target}) => {
    const regExp = /<.*>/g;
    const newValue = target.value.replace(regExp, '');
    target.value = newValue;
}

export const emailValidation = ({target}) =>{
    const re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    const newValue = regExp.test(target.value);
    if (!newValue){console.error("Not valid email")}else{console.log("Ok")};

}
