import {
    CLEAR_COUNTER,
    DECREMENT_COUNTER,
    INCREMENT_COUNTER,
    SET_VALUE_COUNTER,
    SET_CAT_LOADING, ADD_FACT
} from "../actions";

// COUNTER
export const increment = () => ({ type: INCREMENT_COUNTER });
export const decrement = () => ({ type: DECREMENT_COUNTER });
export const clear = () => ({ type: CLEAR_COUNTER });
export const setValue = (value) => ({ type: SET_VALUE_COUNTER, payload: value });

// FACTS
export const setIsLoad = (isLoad) => ({ type: SET_CAT_LOADING, payload: isLoad });

export const addFacts = () => async (dispatch) => {
    dispatch(setIsLoad(true));
    try{
        const { fact } = await fetch('https://catfact.ninja/fact').then(res => res.json());
        dispatch({ type: ADD_FACT, payload: fact });
        dispatch(setIsLoad(false));
    } catch (e) {
        dispatch(setIsLoad(false));
    }

}

