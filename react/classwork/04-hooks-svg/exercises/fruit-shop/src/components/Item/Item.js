import React from 'react';
import styles from './Item.module.scss';
import { ReactComponent as StarRemove } from "../../assets/svg/star-remove.svg";
import { ReactComponent as StarAdd } from "../../assets/svg/star-plus.svg";


const Item = (props) => {
    const { name, price, image, addItem, toggleFav, isFavourite } = props;

    return (
        <div className={styles.root}>
            <div className={styles.favourites} onClick={() => toggleFav(name)} >
                {isFavourite && <StarRemove className={styles.svg}/>}
                {!isFavourite && <StarAdd />}
            </div>
            <p>{ name }</p>
            <img src={image} alt={name} />
            <span>{price}$</span>
            <button onClick={() => {addItem(name, price)}}>Add to cart</button>
        </div>
    )
}

export default Item;