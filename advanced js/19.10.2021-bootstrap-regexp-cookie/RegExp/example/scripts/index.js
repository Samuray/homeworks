const regexp = /[^A-Za-z]/g;
const str = 'Qwerty.1234_)))?!!'
const result = regexp.test(str);
const replacedStr = str.replace(regexp, '');

console.log(result);
console.log(replacedStr);
