class User {
    constructor(name) {
        this.name = name;
    }
}

class Dog {
    constructor(dogName) {
        this.dogName = dogName;
    }
}

export { User as default, Dog };
