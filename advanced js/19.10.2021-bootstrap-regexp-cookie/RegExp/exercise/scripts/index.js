import setTogglePassword from "./togglePassword.js";
import  {nameValidation, commentValidation, emailValidation} from "./validations.js";

setTogglePassword();
document.querySelector('#name').addEventListener('input', nameValidation);
document.querySelector('#comment').addEventListener('blur', commentValidation);
document.querySelector('#email').addEventListener('input', emailValidation);
