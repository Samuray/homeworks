import React from "react";
import Button from "../Button";
import styles from './NoMatch.module.scss';
import {ReactComponent as ErrorPage} from "../../assets/svg/404.svg";
import {Link} from "react-router-dom";

const NoMatch = ({history}) => {
    console.log(history)
    return (
        <section>
            <h1>Sorry, we weren’t able to find the page you are looking for.</h1>
            <ErrorPage style={{width: "50%", height: "50%"}}/>
            {/*<Link to="/">*/}
            {/*    <Button>Go Home</Button>*/}
            {/*</Link>*/}
            <Button onClick={()=>history.replace("/")}>Go Home</Button>
        </section>
    )
}

export default NoMatch;