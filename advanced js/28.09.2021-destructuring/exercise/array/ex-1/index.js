/*
Напишите код, который создает переменные с именем человека, а также с его профессией, выводит запись типа "userName is userProfession";
 */

const user1 = 'Cristian Stuart; developer';
const user2 = 'Archibald Robbins; seaman';
const user3 = 'Zach Dunlap; lion hunter';
const user4 = 'Uwais Johnston; circus artist';


//**@param users (string)


const [name, profession] = (users) => {
    const usersArray = users.split("; ");
    console.log(`${name} is ${profession}`);
}
