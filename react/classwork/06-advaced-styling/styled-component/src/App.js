import './App.css';
import styled, { ThemeProvider } from "styled-components";
import Button from "./components/Button/Button";
import {useState} from "react";
const theme = {
    light: {
        background: '#fff',
        color: '#61dafb',
        borderColor: '#61dafb',
    },
    dark: {
        background: '#61dafb',
        color: '#fff',
        borderColor: '#fff',
    }
};

const Root = styled.section`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  box-sizing: border-box;
  padding: 24px;
`
function App() {
    const [isLightTheme, setIsLightTheme] = useState(true);

  return (
    <div className="App">
        <Root>
            <ThemeProvider theme={isLightTheme ? theme.light : theme.dark}>
                <button onClick={() => setIsLightTheme(current => !current)}>Change Theme</button>
          <Button>Push Me!</Button>
            </ThemeProvider>
        </Root>
    </div>
  );
}

export default App;
