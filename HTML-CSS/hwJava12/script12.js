
"use strict";
let slides = document.querySelectorAll('#slides .image-to-show');
let currentSlide = 0;
let slideInterval = setInterval(nextSlide,3000);

function nextSlide(){
    slides[currentSlide].className = 'image-to-show';
    currentSlide = (currentSlide+1)%slides.length;
    slides[currentSlide].className = 'image-to-show showing';
}

let playing = true;
let pauseButton = document.getElementById('pause');
let playButton = document.getElementById("start");

function pauseSlideshow(){
    pauseButton.innerHTML = 'Прекратить';
    playing = false;
    clearInterval(slideInterval);
}

function playSlideshow(){
    pauseButton.innerHTML = 'Прекратить';
    playing = true;
    slideInterval = setInterval(nextSlide,3000);
}
function pauseSlideshows(){
    playButton.innerHTML = 'Возобновить показ';
    playing = false;
    clearInterval(slideInterval);
}

function playSlideshows(){
    playButton.innerHTML = 'Возобновить показ';
    playing = true;
    slideInterval = setInterval(nextSlide,3000);
}
playButton.onclick = function(){
    if(playing){ pauseSlideshows(); }
    else{ playSlideshows(); }
};
pauseButton.onclick = function(){
    if(playing){ pauseSlideshow(); }
    else{ playSlideshow(); }
};
































