import React, {useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';
import styles from './ShowMore.module.scss';

// Если высота элемента больше 100px - показывать функционал showMore;

const ShowMore = ({ text }) => {
    const paragraphRef = useRef(null);
    const [isShowMore, setIsShowMore] = useState(false);

    useEffect(() => {
        const height = paragraphRef.current.getBoundingClientRect().height;
        if (height < 100) {
            setIsShowMore(false);
        } else {
            setIsShowMore(true)
        }
    }, [])

    return (
        <div className={styles.root}>
            <div className={styles.textWrapper}>
                <p ref={paragraphRef} className={styles.textContainer}>{text}</p>
            </div>
            {isShowMore && <button type='button'>Show</button> }
        </div>
    )
}

ShowMore.propTypes = {
    text: PropTypes.string.isRequired,
};

export default ShowMore;