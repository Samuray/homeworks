
const config = {
    baseURL: 'https://ajax.test-danit.com/api/json',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
}

const instance = axios.create(config);

export default instance;
