const users = require('../../bdMock/users');
const authToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtZXNzYWdlIjoiSldUIFJ1bGVzISIsImlhdCI6MTQ1OTQ0ODExOSwiZXhwIjoxNDU5NDU0NTE5fQ.-yIVBD5b73C75osbmwwshQNRC7frWUYrqaTjTpza2y4';
const { v4: uuidv4 } = require('uuid');

const setToDo = (app) => {
    app.post('/to-do', (req, res) => {

        if (req.headers && !req.headers['authorization']) {
            return res.status(401).send({
                status: `error`,
                error: `Unauthorized: Wrong 'Authorization' header type: expected 'Token <tokenValue>' instead of '${req.headers['authorization']}'`
            })
        }

        if(req.headers && req.headers['authorization']) {
            const [type, token] = req.headers['authorization'].split(' ');

            if (type.toLowerCase() !== 'token'){
                return res.status(401).send({
                    status: `error`,
                    error: `Unauthorized: Wrong 'Authorization' header type: expected 'Token' instead of '${type}'`
                })
            }

            if (token !== authToken){
                return res.status(401).send({
                    status: `error`,
                    error: `Unauthorized: Wrong 'Authorization' header token: '${token}'`
                })
            }
        }

        if(req.headers && req.headers['content-type']) {
            if (!(/application\/json/gi.test(req.headers['content-type']))){
                return res.status(418).send({
                    status: `error`,
                    error: `Wrong Content-type header: expected 'application/json' instead of '${req.headers['content-type']}'`
                })
            }
        }


        const { toDo, userId } = req.body;

        if (!toDo) {
            return res.status(400).send({
                status: `error`,
                error: `toDo is required`
            })
        }

        if (!userId) {
            return res.status(400).send({
                status: `error`,
                error: `userId is required`
            })
        }

        const newToDo = {
            id: uuidv4(),
            text: toDo,
        }

        const index = users.findIndex(({ id }) => id === userId);
        if (index < 0){
             return res.status(404).send({
                 status: `error`,
                 error: `user with id '${userId}' not found`,
             })
        }

        if (users[index] && users[index].toDoList) {
            users[index].toDoList.push(newToDo)
        }

        return res.status(200).send(JSON.stringify({
            status: 'success',
            user: {
                ...users[index]
            },
        }))
    })
}

module.exports = setToDo;
