import Visit from "./Visit.js";

class VisitCard extends Visit {
    constructor(name) {
        super(name);
    }
};

export default VisitCard;
