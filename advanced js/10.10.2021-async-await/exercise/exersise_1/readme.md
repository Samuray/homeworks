# Users

Используя `async/await` получить по указанной URL объекты пользователей (количество на ваше усмотрение)

```js

const url = 'https://randomuser.me/api';

```

<blockquote>
    Сервер отдает на большинтсво запросов ошибки. Постарайтесть получить хотя бы 5-6 положительных ответов.
</blockquote>

Отрисовать в `<div class="container"></div>` карточки типа:

```html

<div class="user">
    <img src="{picture}" alt="{name}">
    <span>Name: {full name}</span>
    <span>Gender: {gender}</span>
    <span>City: {city}</span>
    <span>Tel: {phone}</span>
    <span>Email: {email}</span>
</div>

```

P.S. Телефон и почта должны быть кликабельными ссылками и, соответственно открывать функционал набора номера или отправки письма.

Если нет ни одного положительного ответа - вывести на экран сообщение об этом.
