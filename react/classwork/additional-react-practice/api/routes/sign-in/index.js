const {user, token} = require('../../data');
const {sendBadResponse, sendGoodResponse} = require("../utils");
const {emailRegExp} = require("../../constants/regExp");

const signIn = (app) => {
    app.post('/sign-in', (req, res) => {
        if (!req.body) {
            sendBadResponse(res, 400, `Body is required.`);
            return;
        }

        if (!req.body?.email) {
            sendBadResponse(res, 400, `Email is required.`);
            return;
        }

        if (!emailRegExp.test(req.body?.email)) {
            sendBadResponse(res, 400, `Invalid format of email`);
            return;
        }

        if (!req.body?.password) {
            sendBadResponse(res, 400, `Password is required.`);
            return;
        }

        sendGoodResponse(res, {user, token});
    })
}
module.exports = signIn;