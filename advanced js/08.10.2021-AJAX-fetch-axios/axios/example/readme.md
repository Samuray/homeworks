# AXIOS

[npm](https://www.npmjs.com/package/axios)  
[docs](https://axios-http.com/docs/intro)

Axios — это широко известная JavaScript-библиотека. Она представляет собой HTTP-клиент, основанный на промисах и предназначенный для браузеров и для Node.js.

## GET

```js

axios.get('https://catfact.ninja/fact')
    .then(response => console.log(response))
    .catch(error => console.error(error));

```

## POST 

```js

const body = {
    title: 'Post',
    body: 'SomePost',
}

axios.post('https://ajax.test-danit.com/api/json/posts', body)
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });

```

### [Еще методы](https://axios-http.com/docs/api_intro)
### [Конфиг](https://axios-http.com/docs/req_config)
### [Схема ответа](https://axios-http.com/docs/res_schema)


## AXIOS Instance

Мы можем создавать собственный экземпляр axios с настройками по умолчанию.

```js

const config = {
    baseURL: 'https://ajax.test-danit.com/api/json',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
}

const instance = axios.create(config);


instance.get('/users').then(({ data }) => console.log(data));
instance.get('/posts ').then(({ data }) => console.log(data));
instance.get('/comments').then(({ data }) => console.log(data));
instance.get('/photos ').then(({ data }) => console.log(data));

```

## [Интерцепторы](https://axios-http.com/docs/interceptors)

Мы можем перехватывать `response` до того как он попадет в `then` или `catch`

```js

axios.interceptors.request.use(config => {
    // Сделать что-то до того как запрос будет отправлен
    config.headers['Authorisation'] = 'Basic YWxhZGRpbjpvcGVuc2VzYW1l';
    return config;
  }, error => {
    // Сделать что-то c ошибкой отправки запроса
    return Promise.reject(error);
  });

axios.interceptors.response.use(response => {
    // Сделать что-то до того как reponse попадет в then
    response.data.fe30 = 'Amazing group';
    return response;
}, error => {
    // Сделать что-то c ошибкой отправки запроса
    return Promise.reject(error);
});

```

### Отличия на примерах - [статья](https://habr.com/ru/company/ruvds/blog/477286/)
