/**
 * @param str {string}
 * @returns {string}
 */
const removeLetter = (str) => {
    if (typeof str !== "string") {
        return null;
    }
    return str.replace(/[A-Za-z]/gi, '');
}

export default removeLetter;