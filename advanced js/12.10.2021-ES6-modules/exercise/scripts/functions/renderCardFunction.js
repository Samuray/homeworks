import {Card} from "../classes/Card.js"
import {getPosts} from "../API/getPosts.js"

const renderCardWithData = (postContainer, data) => {
            data.forEach( ({title, body}) =>{
                postContainer.append(new Card(title, body).render())
            });
};



/**
 * @param postContainer {HTMLBaseElement}
 */
const renderCardFunction = async  (postContainer) => {
    try{
        const {status, data} = await getPosts()
    if (status===200){
        data.forEach( ({title, body}) =>{
            postContainer.append(new Card(title, body).render())
        });
    }
    }catch(error){
        console.log(error);
    }
};
export {renderCardFunction, renderCardWithData};
