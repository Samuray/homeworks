## Installations
`yarn` or `npm i`

## Scripts
`yarn start` or `npm run start` - start express server on 3000 port;

[http://localhost:3000/](http://localhost:3000/)

## Descriptions

The server responds one request after 1 second and the next one after 30 seconds and again and again.

The task is to implement the processing of a normal and hung (too long) response.
