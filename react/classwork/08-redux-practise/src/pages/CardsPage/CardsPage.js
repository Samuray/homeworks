import React from "react";
import CardContainer from "../../components/Cards/CardContainer";
import CreateCardForm from "../../components/Cards/CreateCardForm";


function CardsPage({ setCartItems, setNotificationConfig }) {

    return (
        <section>
            <h1>CARDS</h1>
            <CreateCardForm />
            <CardContainer setCartItems={setCartItems} setNotificationConfig={setNotificationConfig}/>
        </section>
    )
}

export default CardsPage;