import styles from './App.module.scss';
import Header from "./components/Header";
import {BrowserRouter} from "react-router-dom";
import Routes from "./Routes";
import Modal from "./components/Modal";

function App() {

  return (
      <BrowserRouter>
        <div className={styles.app}>
          <Header />
          <section>
            <Routes/>
          </section>
        </div>
      </BrowserRouter>
  );
}

export default App;
