import React, {Component} from "react";
import style from './Footer.module.scss'

const Footer = (props) => {
        const {title, countMessage } = props;
        return(
            <footer className={style.footer}>
                <span className={style.title}>{title}</span>
                <span>Emails: {countMessage}</span>
            </footer>
        );

}

export default Footer
