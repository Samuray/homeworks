import {renderCardFunction} from "./functions/renderCardFunction.js";
import filterCards from "./functions/filterCards.js";
renderCardFunction(document.querySelector('.post-container'));
import throttle from "./utils/throttle.js";

document.querySelector("#search").addEventListener("input", throttle(({currentTarget}) => {
    filterCards(currentTarget.value);
}, 1000))
