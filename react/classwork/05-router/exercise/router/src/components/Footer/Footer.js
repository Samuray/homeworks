import React from 'react';
import Button from "../Button";
import styles from './Footer.module.scss';

const Footer = (props) => {
        const { title, year, onOrderFunc } = props;

        return (
              <footer className={styles.root}>
                  <span>{title}</span>
                  <span>FE-30, {year}</span>
                  <Button onClick={onOrderFunc}>Order Call</Button>
              </footer>
        );
};


export default Footer;
