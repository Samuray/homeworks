document.querySelector(".form").addEventListener("submit", (e) => {
    e.preventDefault();
    const body = {};
    document.querySelectorAll("input").forEach(({name, value}) => {
        body[name] = value;
    })
    axios.post('http://localhost:3000/log-in', body,{
        headers: {
            "Content-Type": "application/json"
        }
    }).then(({data, status}) => {
        if (status === 200) {
            console.log(data)

            localStorage.setItem("user", JSON.stringify(data.data));
            localStorage.setItem("authToken", data.authToken);

            location.href = location.href.split('/sign-in')[0];
        }
    }).catch(error => console.log(error))
})

