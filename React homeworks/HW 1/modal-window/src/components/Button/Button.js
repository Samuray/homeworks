import React from 'react';
import styles from "./Button.module.scss"


export default class Button extends React.Component {
    render() {
        return (
            <button
                className={styles.borderButton}
                onClick={this.props.onClick}
                style={{backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : "rgba(0, 0, 0, 0.5)"}}>
                {this.props.text}
            </button>
        )
    }
}
