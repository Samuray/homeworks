`use strict`;
const price = document.getElementById("Price");
const p = document.createElement("p");
const span = document.createElement("span");
const btn = document.createElement("button");
const parent = document.getElementById("span");
p.innerText = " Please enter correct price!"


price.addEventListener("focus",() => {
price.classList.add("focus");
const text = document.querySelector("p");
if (text){
text.remove()
}
})

price.addEventListener('blur', (event)=> {
 let val = price.value;
 if (Math.sign(val) === -1 || isNaN(val) || val === "") {
  price.classList.remove("focus")
  price.classList.add("invalid");
  price.after(p);
 }
 else {
  price.classList.add("valid");
  span.innerText = ` Текущая цена: $ ${val} `
  btn.innerText = "x";
  span.appendChild (btn);
  parent.appendChild(span);
  price.classList.add("color");
 }
})

btn.addEventListener("click", () => {
 span.remove();
 price.value = "";
 location.reload();
 });

