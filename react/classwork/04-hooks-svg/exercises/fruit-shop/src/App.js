import React, { useEffect, useState } from "react";
import ItemsContainer from "./components/ItemsContainer";
import Cart from "./components/Cart";
import styles from './App.module.scss';

function App() {
    const [fruits, setFruits] = useState([]);
    const [cartItems, setCartItems] = useState([])
    console.log(fruits)

    useEffect(() => {
        (async () => {
            const { data } = await fetch("http://localhost:3001/items").then(res => res.json());
            setFruits(data)
        })()

    }, [])

    useEffect(() => {
        console.log(fruits)
    }, [fruits])

    const toggleFav = (name) => {
        const index = fruits.findIndex(({name: arrayName}) => {
            return name === arrayName;
        })
        setFruits((current) => {
            const newState = [...current];
            newState[index].isFavourite = !newState[index].isFavourite ;
            return newState;
        })
    }

    const addItem = (name, price) => {
      const index = cartItems.findIndex(({name: arrayName}) => {
         return name === arrayName;
      })
        if (index === -1) {
            setCartItems((current) => [...current, {name, price, count: 1}])
        } else {
            setCartItems((current) => {
                const newState = [...current];
                newState[index].count = newState[index].count +1;
                return newState;
            })
        }
    }

  return (
    <div className={styles.root}>

      <div>
        <ItemsContainer toggleFav={toggleFav} items={fruits} addItem={addItem} />
      </div>

      <div>
        <Cart items={cartItems}
        />
      </div>
    </div>
  );
}

export default App;
