import './App.scss';
import Header from "./components/Header";
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./Routes";
import { Provider } from "react-redux";
import appStore from "./store/appStore";

function App() {
  return (
      <Provider store={appStore}>
          <Router>
            <div className="App">
                <Header />
                <section>
                    <Routes />
                </section>
            </div>
          </Router>
      </Provider>
  );
}

export default App;
