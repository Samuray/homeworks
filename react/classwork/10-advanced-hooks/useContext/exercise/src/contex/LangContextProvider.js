import LangContext from "./LangContext";
import PropTypes from "prop-types";
import {useState} from "react";

const LangContextProvider = (props) => {
    const { children } = props;
    const [value, setValue] = useState('EN');

    const langValue = {
        value,
        setValue
    }

    return (
        <LangContext.Provider value={value}>
            {children}
        </LangContext.Provider>
    )
}

LangContextProvider.propTypes = {
    children: PropTypes.arrayOf(PropTypes.node).isRequired,
    value: PropTypes.any,
}

LangContextProvider.defaultProps = {
    value: 'EN',
}

export default LangContextProvider;