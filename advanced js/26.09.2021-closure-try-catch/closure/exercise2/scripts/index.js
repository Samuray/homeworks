/*

Напишите набор готовых функций-фильтров для array.filter();

1. inBetween(a, b) – находится между a и b (включительно).
2. inArray([...]) – находится в данном массиве [...].

 */

const array = [1, 3, 2, 4, 4, 5, 6, 11, 2, 31];

const inBetween = (a, b) => {

    return (elem) => elem >= a && elem <= b;

};
const inArray = (array) => {
    return(elem) => array.includes (elem);
};

array.filter(inBetween(2, 7));