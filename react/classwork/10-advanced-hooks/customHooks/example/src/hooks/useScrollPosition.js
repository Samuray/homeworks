import {useState, useEffect, useRef} from 'react';

const useScrollPosition = (timeout = 300) => {
    const [scrollPosition, setScrollPosition] = useState(0);
    const debounceRef = useRef(false)

    const onScroll = () => {
        if (!debounceRef.current) {
            debounceRef.current = true;
            setTimeout(() => debounceRef.current = false, timeout);
            setScrollPosition(window.scrollY);
            console.log(window.scrollY);
        }
    }

    useEffect(() => {
            window.addEventListener('scroll', onScroll, { passive: true });

            return () => {
                window.removeEventListener('scroll', onScroll);
            }
    }, [])

    return scrollPosition;
}

export default useScrollPosition;