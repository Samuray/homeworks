import React from 'react';
import styles from './Header.module.scss';
import useScrollPosition from "../../hooks/useScrollPosition";
import classNames from "classnames";

const Header= () => {
    const scrollPosition = useScrollPosition(10);
    const isShadow = scrollPosition > 100;

    return (
              <header className={classNames(styles.root, { [styles.scrolled]: isShadow })}>

              </header>
        );
};

export default Header;
