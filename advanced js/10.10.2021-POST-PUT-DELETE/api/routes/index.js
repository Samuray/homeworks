const register = require('./register');
const logIn = require('./log-in');
const toDo = require('./to-do');

module.exports = (app) => {
    register(app);
    logIn(app);
    toDo(app)
}
