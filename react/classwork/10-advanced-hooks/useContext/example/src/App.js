import React, {useState} from "react";
import './App.scss';
import Header from "./components/Header";
import Routes from "./Routes";
import {BrowserRouter} from "react-router-dom";
import classNames from "classnames";
import darkTheme from './styles/dark.module.scss';
import lightTheme from './styles/light.module.scss';
import AppContextProvider from "./context/AppContextProvider";

function App() {
    const [isLight, setIsLight] = useState(true);
    const themeStyles = isLight ? lightTheme : darkTheme;

    const contextValue = {
        themeStyles,
        setIsLight,
        isLight
    }

    return (
        <AppContextProvider value={contextValue}>
            <BrowserRouter>
                <div className={classNames("App", themeStyles.App)}>
                    <Header title="React Context"/>
                    <Routes/>
                </div>
            </BrowserRouter>
        </AppContextProvider>
    );
}

export default App;
