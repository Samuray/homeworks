import React, {useEffect, useState} from "react";
import {useRouteMatch} from "react-router-dom";

function PostPage() {
    const {params: { id }} = useRouteMatch();

    const [post, setPost] = useState({});

    useEffect(() => (async () => {
        const result = await fetch(`https://ajax.test-danit.com/api/json/posts/${id}`)
            .then(res => res.json());
        setPost(result);
    })(), []);

    return (
        <section>
            {post.id &&
            <>
                <h2>{post.title}</h2>
                <p>{post.body}</p>
            </>}
        </section>
    )
}

export default PostPage;