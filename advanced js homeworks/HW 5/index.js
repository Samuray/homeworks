
document.addEventListener('DOMContentLoaded', async function (e) {
    const usersData = await fetch('https://ajax.test-danit.com/api/json/users').then(r => r.json());
    const userList = usersData.map(item => new User(item));

    const postsData = await fetch('https://ajax.test-danit.com/api/json/posts').then(r => r.json());
    const postList = postsData.map(item => new Post({
        ...item,
        user: userList.find(user => user.getUserID() === item.userId)
    }));

    postList.map(item => new PostCard(item.user, item.title, item.body, item.id).render())
});

class User {
    #id;

    constructor({id, name, email}) {
        this.#id = id;
        this.name = name;
        this.email = email;
    }

    getUserID() {
        return this.#id;
    }
}

class Post {
    constructor({id, user, title, body}) {
        this.id = id;
        this.user = user;
        this.title = title;
        this.body = body;
    }
}

class PostCard {
    constructor(user, title, body, id) {
        this.user = user
        this.title = title
        this.body = body
        this.id = id
        this.container = document.createElement('div');
        this.buttonElem = document.createElement('button');
        this.section = document.querySelector(".widgets");
    }

    render(selector) {
        this.container.innerHTML = ` <img class="widgets__widgetContainer-img" src="./img/Avatar.jpg" alt="img"/>
                            <img class="widgets__widgetContainer-icon" src="./img/Twitter.png" alt="icon"/>
                            <span class="widgets__widgetContainer-title">${this.user.name},${this.user.email}</span>
                           <span class="widgets__widgetContainer-text">${this.title}</span>

                                <div class="widgets__content">
                                    <img class="widgets__content-foto" src="./img/tachka.jpg" alt="image"/>
                                   <span class="widgets__content-text">${this.body}</span>
                                </div>

                           <div class="widgets__section">
                               <img src="./img/otvet.png" alt="icon"/>
                               <p>10 тыс</p>
                                <img src="./img/arrow_arrows.png" alt="icon"/>
                                <p>15 тыс</p>
                               <img src="./img/serdce.png" alt="icon"/>
                               <p>150 тыс</p>
                               <img src="./img/loading.png" alt="icon"/>
                           </div>  `;
        this.container.className = "widgets__widgetContainer";
        this.container.appendChild(this.buttonElem);

        this.buttonElem.className = 'btn';
        this.buttonElem.innerHTML = 'DELETE';
        this.buttonElem.addEventListener('click', async () => {
            const result = await fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
                method: "DELETE",
                  "Content -Type": "application / json" ,
                "Access-Control-Allow-Credentials" : true,
                "Access-Control-Allow-Origin" : "*" ,

            })
                .then(r => r.text());
            if (result === "") {
                console.log(result)
                this.container.remove();
            }
        })
        this.section.appendChild(this.container);
    }
}



// const userCards = async () => {
//  const userData = await axios.get('https://ajax.test-danit.com/api/json/users');
//  const dataUser = userData.data;
//  console.log(dataUser);
//
//  const userPosts = await axios.get('https://ajax.test-danit.com/api/json/posts');
//  const dataPosts = userPosts.data;
//  console.log(dataPosts);
//
//  dataUser.forEach(({name,email, id})=> {
//  const res = dataPosts.filter(item => item )
//   console.log(res)
//   document.querySelector(".widgets").insertAdjacentHTML("beforeEnd", `<div class="widgets__widgetContainer">
//
//                             <img class="widgets__widgetContainer-img" src="./img/Avatar.jpg" alt="img"/>
//                             <img class="widgets__widgetContainer-icon" src="./img/Twitter.png" alt="icon"/>
//                             <span class="widgets__widgetContainer-title">${name},${email}</span>
//                            <span class="widgets__widgetContainer-text"></span>
//
//                                 <div class="widgets__content">
//                                     <img class="widgets__content-foto" src="./img/tachka.jpg" alt="image"/>
//                                    <span class="widgets__content-text"></span>
//                                 </div>
//
//                            <div class="widgets__section">
//                                <img src="./img/otvet.png" alt="icon"/>
//                                <p>10 тыс</p>
//                                 <img src="./img/arrow_arrows.png" alt="icon"/>
//                                 <p>15 тыс</p>
//                                <img src="./img/serdce.png" alt="icon"/>
//                                <p>150 тыс</p>
//                                <img src="./img/loading.png" alt="icon"/>
//                            </div>
//
//                             <button class="btn">DELETE</button>
//                        </div>`)
//     })
//
// }
// userCards()



// document.addEventListener('DOMContentLoaded', async function (e) {
//     const userData = await axios.get('https://ajax.test-danit.com/api/json/users');
//     const dataUser = userData.data;
//     console.log(dataUser);
//
//     const userPosts = await axios.get('https://ajax.test-danit.com/api/json/posts');
//     const dataPosts = userPosts.data;
//     console.log(dataPosts);
//
//     dataUser.forEach(({name, email, id}) => {
//         const res = dataPosts.filter(item => dataPosts(item))
//         console.log(res)
//
//     })
// })

// class User {
//     #id;
//
//     constructor({id, name, email}) {
//         this.#id = id;
//         this.name = name;
//         this.email = email;
//     }
//
//     getUserID() {
//         return this.#id;
//     }
// }
//
// class Post {
//     constructor({id, user, title, body}) {
//         this.id = id;
//         this.user = user;
//         this.title = title;
//         this.body = body;
//     }
// }
//
// class PostCard {
//     constructor(user, title, body, id) {
//         this.user = user
//         this.title = title
//         this.body = body
//         this.id = id
//     }
//
//     render() {
//         document.querySelector(".widgets").insertAdjacentHTML("beforeend", `<div class="widgets__widgetContainer">
//
//                             <img class="widgets__widgetContainer-img" src="./img/Avatar.jpg" alt="img"/>
//                             <img class="widgets__widgetContainer-icon" src="./img/Twitter.png" alt="icon"/>
//                             <span class="widgets__widgetContainer-title">${this.user.name},${this.user.email}</span>
//                            <span class="widgets__widgetContainer-text">${this.title}</span>
//
//                                 <div class="widgets__content">
//                                     <img class="widgets__content-foto" src="./img/tachka.jpg" alt="image"/>
//                                    <span class="widgets__content-text">${this.body}</span>
//                                 </div>
//
//                            <div class="widgets__section">
//                                <img src="./img/otvet.png" alt="icon"/>
//                                <p>10 тыс</p>
//                                 <img src="./img/arrow_arrows.png" alt="icon"/>
//                                 <p>15 тыс</p>
//                                <img src="./img/serdce.png" alt="icon"/>
//                                <p>150 тыс</p>
//                                <img src="./img/loading.png" alt="icon"/>
//                            </div>
//
//                             <button class="btn">DELETE</button>
//                        </div>`)
//
//          document.querySelector(".btn").addEventListener("click", async  () =>  {
//              await fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
//                 method: "DELETE"
//             })
//                 new PostCard(this.id).remove()
//         })
//     }
// }








