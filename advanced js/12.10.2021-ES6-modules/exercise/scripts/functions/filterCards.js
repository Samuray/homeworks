import {getPosts} from "../API/getPosts.js";
import {Card} from "../classes/Card.js";
import {renderCardWithData} from "./renderCardFunction.js";
import debounce from "../utils/debounce.js";

async function filterCards(value){
    try{
        const {status, data} = await getPosts()
        if (status===200){
            const filteredPosts = data.filter(({title}) => {
                return title.includes(value);
            })

            document.querySelector('.post-container').innerHTML = '';
            renderCardWithData(document.querySelector('.post-container'), filteredPosts);

        }
    }catch(error){
        console.log(error);
    }
}
export default filterCards;
