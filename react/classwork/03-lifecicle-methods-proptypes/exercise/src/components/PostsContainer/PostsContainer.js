import React, { PureComponent } from 'react';
import Post from "../Post";
import styles from './PostsContainer.module.scss';
import Preloader from "../Preloader";

class PostsContainer extends PureComponent {
    render(){
        const { posts, isLoading, isError } = this.props;

        return (
              <section className={styles.root}>
                  <h1>POSTS</h1>
                      <div className={styles.postsContainer}>
                          {!isLoading ? posts.map(({id, ...args}) => <Post key={id} {...args} />) :
                          <Preloader />}
                          {isError && <h1>Error!</h1>}
                      </div>
              </section>
        );
    }
};


export default PostsContainer;
