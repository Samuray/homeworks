import React, {useEffect} from "react";
import {Switch, Route, Redirect, useLocation} from 'react-router-dom'
import HomePage from "../pages/HomePage";
import BlogPage from "../pages/BlogPage";
import PostPage from "../pages/PostPage";
import NoMatchPage from "../pages/NoMatchPage";
import SignPage from "../pages/SignPage";

function Routes({ titleFromApp }){
    const {pathname} = useLocation();

    const isAuth = localStorage.getItem('isAuth') === 'true';

    useEffect(() => {
        console.log(pathname);
    }, [pathname]);


    return(
        <Switch>

            {/*<Route path='/'>*/}
            {/*    <HomePage />*/}
            {/*</Route>*/}

            {/*<Route exact path="/" component={HomePage} />*/}

            {/*<Route */}
            {/*    exact path="/" */}
            {/*    render={({ history, location, match }) => {*/}
            {/*        <HomePage history={history} location={location} somElse='sdhfskhdfgkshdgfhsgdjfgs' />*/}
            {/*    }} />*/}


            <Route exact path="/posts" component={BlogPage} />

            <Route exact path="/posts/:id" component={PostPage} />

            {/*<Route exact path="/" component={HomePage} />*/}
            {/*<Route exact path="/" render={(props) => {*/}
            {/*    const {history} = props;*/}
            {/*    console.log('history', history);*/}
            {/*    return <HomePage { ...props } title={titleFromApp} />*/}
            {/*}} />*/}
            <Route exact path="/">
                {isAuth ?
                    <Redirect to="/home" /> :
                    <Redirect to="/sign" />}
            </Route>

            <Route exact path="/home">
                {/*{isAuth ?*/}
                {/*    <HomePage title={titleFromApp} /> :*/}
                {/*    <Redirect to="/sign" />}*/}

                <HomePage title={titleFromApp} />

            </Route >

            <Route exact path="/sign">
                <SignPage />
            </Route>

            <Route exact path="/">
                <Redirect to='/home' />
            </Route>

            <Route path="*" component={NoMatchPage}/>
        </Switch>
    );
}

export default Routes