_**Подготовительное задание:**_ Создать репозиторий, настроить protected branch, добавить в develop файлы из папки ```template```
1. Клонируем себе репозиторий.
2. Создаем новую ветку от ```develop``` согласно методологии git flow со своим именем и фамилией в названии.
3. Изменяем в файле ```src/styles/shared/variables``` переменные цветов на любые другие. Пушим их в свою удаленную ветку в репозиторий;
4. Преподаватель в ветке ```feature/new-color-variables``` обновляет переменные цветов. Мерджим в свою ветку новые изменения, решаем конфликт в сторону новых цветов от преподавателя.
5. Создаем в ```index.html``` новый параграф с уникальным ```id``` из имени и фамилии с текстом имени и фамилии. Например
    ```html
    <p id="ivan_ivanov">Иван Иванов</p>
    ```
   И прописываем для этого параграфа уникальные стили в файле ```src/styles/components/main-container```. Например: 
    ```scss
    #ivan_ivanov {
      color: #6E6893;
      font-size: 45px;
      font-weight: 800; 
   }
    ```
6. Пушим изменения в свою ветку в репозиторий и создаем merge request в develop на своего соседа.
7. Проверяем merge request, ставим лайк если все ок, мерджим в develop.
8. Создаем [релиз](http://risovach.ru/upload/2015/03/mem/vypem-za-lyubov_76568943_orig_.jpg) :)
