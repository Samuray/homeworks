export class Modal {
    constructor(element, onSubmitAction) {
        this.onSubmitAction = onSubmitAction;
        this.element = element;
        this.container = document.createElement("div");
        this.background = document.createElement("div");
        this.mainContainer = document.createElement("div");
        this.closeButton = document.createElement("button");
        this.contentWrapper = document.createElement("div");
        this.buttonWrapper = document.createElement("div");
        this.buttonYes = document.createElement('button');
    }
    createElements() {
        this.container.classList.add('modal');
        this.background.classList.add('modal__background');
        this.mainContainer.classList.add('modal__main-container');
        this.closeButton.classList.add('modal__close');
        this.contentWrapper.classList.add('modal__content-wrapper');
        this.buttonWrapper.classList.add('modal__button-wrapper');
        this.buttonYes.classList.add('modal__confirm-btn');

        this.buttonYes.innerHTML = 'Yes';

        this.mainContainer.append(this.closeButton);
        this.mainContainer.append(this.contentWrapper);
        this.mainContainer.append(this.buttonWrapper);
        this.buttonWrapper.append(this.buttonYes);
        this.contentWrapper.append(this.element);


        this.container.append(this.mainContainer);
        this.container.append(this.background);

        this.closeButton.addEventListener('click', this.closeMe.bind(this));
        this.background.addEventListener('click', this.closeMe.bind(this));

        this.buttonYes.addEventListener('click', () => {
            this.onSubmitAction();
            this.closeMe();
        } );
    }

    closeMe() {
        this.container.remove();
    }

    render(selector = 'body') {
        this.createElements();
        document.querySelector(selector).append(this.container);
    }
}


