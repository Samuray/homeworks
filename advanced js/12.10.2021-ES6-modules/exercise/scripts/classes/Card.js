class Card {
    constructor(title, text) {
        this.title = title;
        this.text = text;
        this.container=document.createElement("div");
    }
    render(){
        this.container.insertAdjacentHTML("beforeend",`
            <h3>${this.title}</h3>
            <p>${this.text}</p>`);
        this.container.classList.add("card")
        return this.container;
            };
}

export {Card};
