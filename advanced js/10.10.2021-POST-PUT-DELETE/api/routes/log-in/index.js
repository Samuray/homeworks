const users = require('../../bdMock/users');
const emailRegExp = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
const authToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtZXNzYWdlIjoiSldUIFJ1bGVzISIsImlhdCI6MTQ1OTQ0ODExOSwiZXhwIjoxNDU5NDU0NTE5fQ.-yIVBD5b73C75osbmwwshQNRC7frWUYrqaTjTpza2y4';

const logIn = (app) => {
    app.post('/log-in', (req, res) => {

        if(req.headers && req.headers['content-type']) {
            if (!(/application\/json/gi.test(req.headers['content-type']))){
                return res.status(418).send({
                    status: `error`,
                    error: `Wrong Content-type header: expected 'application/json' instead of '${req.headers['content-type']}'`
                })
            }
        }


        const { email, password } = req.body;

        if (!email) {
            return res.status(400).send({
                status: `error`,
                error: `Email is required`
            })
        }

        if (!password) {
            return res.status(400).send({
                status: `error`,
                error: `Password is required`
            })
        }

        if (!emailRegExp.test(email)) {
            return res.status(400).send({
                status: `error`,
                error: `Invalid format of Email`
            })
        }

        const user = users.find(({ email: userEmail }) => userEmail === email);

        if (!user) {
            return res.status(404).send({
                status: `error`,
                error: `User with email '${email}' not found`
            })
        }

        if (user.password !== password) {
            return res.status(401).send({
                status: `error`,
                error: `Wrong password for '${email}'`
            })
        }

        const {password: _, ...rest} = user;
        return res.status(200).send(JSON.stringify({
            status: 'success',
            data: {
                ...rest
            },
            authToken,
        }))
    })
}

module.exports = logIn;
