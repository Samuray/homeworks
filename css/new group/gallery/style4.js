
const images = [
    {
        src: 'images/corgi/1.jpg',
        category: 'corgi'
    },    {
        src: 'images/corgi/2.jpg',
        category: 'corgi'
    },    {
        src: 'images/corgi/3.jpg',
        category: 'corgi'
    },    {
        src: 'images/corgi/4.jpg',
        category: 'corgi'
    },    {
        src: 'images/corgi/5.jpg',
        category: 'corgi'
    },    {
        src: 'images/corgi/6.jpg',
        category: 'corgi'
    },    {
        src: 'images/corgi/7.jpg',
        category: 'corgi'
    },    {
        src: 'images/corgi/8.jpg',
        category: 'corgi'
    },    {
        src: 'images/dachshund/1.jpg',
        category: 'dachshund'
    },{
        src: 'images/dachshund/2.jpg',
        category: 'dachshund'
    },{
        src: 'images/dachshund/3.jpg',
        category: 'dachshund'
    },{
        src: 'images/dachshund/4.jpg',
        category: 'dachshund'
    },{
        src: 'images/dachshund/5.jpg',
        category: 'dachshund'
    },{
        src: 'images/dachshund/6.jpg',
        category: 'dachshund'
    },{
        src: 'images/dachshund/7.jpg',
        category: 'dachshund'
    },{
        src: 'images/dachshund/8.jpg',
        category: 'dachshund'
    },{
        src: 'images/basset/1.jpg',
        category: 'basset'
    },{
        src: 'images/basset/2.jpg',
        category: 'basset'
    },{
        src: 'images/basset/3.jpg',
        category: 'basset'
    },{
        src: 'images/basset/4.jpg',
        category: 'basset'
    },{
        src: 'images/basset/5.jpg',
        category: 'basset'
    },{
        src: 'images/basset/6.jpg',
        category: 'basset'
    },{
        src: 'images/basset/7.jpg',
        category: 'basset'
    },{
        src: 'images/basset/8.jpg',
        category: 'basset'
    },
]

let perPage = 4;
let currentCategory
const mainContainer = document.querySelector(".grid")
console.log(mainContainer);

const render = (arr) => {
    const mainContainer = document.querySelector(".grid");
    const sliceArray = arr.slice(0, perPage);
    const htmlArray = sliceArray.map((element) => {
        return `<img class="grid-item" src="${element.src}">`
    });
    mainContainer.innerHTML = htmlArray.join(" ");
    const btn = document.querySelector(".load-more")
    if ( perPage >= arr.length) {
        btn.classList.add ("visible")
    }
    else {
        btn.classList.remove("visible")
    }
}
render(images);

let filteredArr = (arr,category) => {
    arr.filter((e) => e.category === category)
}
console.log(filteredArr(images));

const tabsContainer = document.querySelector(".tabs");
tabsContainer.addEventListener("click", (event) => {
    const category = event.target.dataset.category
    currentCategory = category;
    perPage = 4;
    if (category === "all" && event.target !== event.currentTarget) {
        render(images)
    }
    else {
        const newArr = filteredArr(images, category);
        render(newArr);
    }
    const tabs = document.querySelectorAll(".tab")
tabs.forEach(e => {
    e.classList.remove("active");
});
    event.target.classList.add ("active");

});
const button = document.querySelector(".load-more");
button.addEventListener("click", () => {
    perPage = perPage + 4;
    if ( currentCategory === 'all') {
        render(images);
    }
    else {
        render(filteredArr(images, currentCategory));
    }
})

  


