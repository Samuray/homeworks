import {Component} from "react";
import styles from "./Modal.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";

 class Modal extends Component {
  render() {
    return (
      <div onClick={this.props.isOpen} className={styles.modal}>
        <div
          onClick={(event) => event.stopPropagation()}
          className={styles.modal__content}
        >
          <div className={styles.modal__header}>
            <h5 className={styles.modal__title}>{this.props.header}</h5>
            {this.props.closeButton && (
              <span onClick={this.props.isOpen}>&#10761;</span>
            )}
          </div>
          <div className={styles.modal__body}>
            <p className={styles.body__text}>{this.props.textModal}</p>
          </div>
          {this.props.actions}
        </div>
      </div>
    );
  }
}

Button.propTypes = {
  isOpen: PropTypes.func,
  header: PropTypes.string,
  textModal: PropTypes.string,
  closeButton: PropTypes.bool,
  actions: PropTypes.object,
};
Button.defaultProps = {
  closeButton: true,
};

export default Modal;