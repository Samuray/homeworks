# Контекст выполнения (execution context)
это, если говорить упрощённо, концепция, описывающая окружение, в котором производится выполнение кода на JavaScript. Код всегда выполняется внутри некоего контекста.

## В JavaScript существует три типа контекстов выполнения:

* **_Глобальный контекст выполнения._**   
Это базовый, используемый по умолчанию контекст выполнения. Если некий код находится не внутри какой-нибудь функции, значит этот код принадлежит глобальному контексту. Глобальный контекст характеризуется наличием глобального объекта, которым, в случае с браузером, является объект window, и тем, что ключевое слово this указывает на этот глобальный объект. В программе может быть лишь один глобальный контекст.


* **_Контекст выполнения функции._**  
Каждый раз, когда вызывается функция, для неё создаётся новый контекст. Каждая функция имеет собственный контекст выполнения. В программе может одновременно присутствовать множество контекстов выполнения функций. При создании нового контекста выполнения функции он проходит через определённую последовательность шагов, о которой мы поговорим ниже.


* _**Контекст выполнения функции eval.**_  
Код, выполняемый внутри функции eval, также имеет собственный контекст выполнения. P.S. Не используйте функцию eval;


# This
Свойство контекста выполнения кода (global, function или eval), которое в нестрогом режиме всегда является ссылкой на объект, а в строгом режиме может иметь любое значение.

## Global контекст

В глобальном контексте выполнения (за пределами каких-либо функций) this ссылается на глобальный объект вне зависимости от режима (строгий или нестрогий).

```js
console.log(this === window); // true

a = 37;
console.log(window.a); // 37

this.group = "FE-30";
console.log(window.group)  // "FE-30"
console.log(group)         // "FE-30"
```

Вы всегда можете легко получить объект ```global```, используя глобальное свойство ```globalThis```, независимо от текущего контекста, в котором выполняется ваш код.

## Function контекст

Не в строгом режиме значение this не устанавливается вызовом и по умолчанию будет использоваться объект global, которым в браузере является window.

```js
function logThis(){
    console.log(this);
}

logThis(); // window
```

В строгом режиме, если значение this не установлено в контексте выполнения, оно остаётся undefined


## В методе объекта

Когда функция вызывается как метод объекта, используемое в этой функции ключевое слово this принимает значение объекта, по отношению к которому вызван метод.

```js
function logThis(){
    console.log(this);
}

const someObject = {
    someString: 'Hello, I\'m pretty string ;)',
    someNumber: 30,
    isItAwesomeObject: true,
    logThis,
}

someObject.logThis(); // someObject
```

## Стрелочные функции

В стрелочных функциях, this привязан к окружению, в котором была создана функция.  

Стрелочные функции не содержат собственный контекст this, а используют значение this окружающего контекста.
* В глобальной области видимости this будет указывать на глобальный объект (правила "use strict" относительно this игнорируются).
* Если стрелочная функция вызвана из другой функции - this будет равняться this родителя. 

```js
const logArrowFunctionThis = () => console.log('arrowFunction: ', this);
logArrowFunctionThis(); // window


function globalFunction(){
    console.log(this)
    console.log('global function');
    logArrowFunctionThis();
}
globalFunction() // window
```

```js
const someObject = {
    someString: 'Hello, I\'m pretty string ;)',
    someNumber: 30,
    isItAwesomeObject: true,
    logArrowFunction: () => console.log(this),
    logThis(){
        const logInnerArrowFunction = () => {
            console.log('logInnerArrowFunction:', this);
        }

        const logInnerFunction = function(){
            console.log('logInnerFunction:', this);
        }

        logInnerArrowFunction();
        logInnerFunction();
    },
}
someObject.logArrowFunction(); // window
someObject.logThis();
```


## Метод ```call```

Метод call() вызывает функцию с указанным значением this и индивидуально предоставленными аргументами.

```js
func.call(context, arg1, arg2, ...)

// thisArg - Значение this, предоставляемое для вызова функции func.
// arg1, arg2, ... - Аргументы для func.
```

```js
const showFullName = function(age, city){
    alert(`Name: ${this.firstName}, and lastname: ${this.lastName}, age: ${age}, city: ${city}`);
}

const user = {
    firstName: 'Brendan',
    lastName: 'Eich',
    
}

showFullName.call(user, 31, 'Pittsburgh');
```


## Метод ```apply```

Если нам неизвестно, с каким количеством аргументов понадобится вызвать функцию, можно использовать более мощный метод: apply.

Вызов функции при помощи func.apply работает аналогично func.call, но принимает массив аргументов вместо списка.


## Метод ```bind```

Метод bind() создаёт новую функцию, которая при вызове устанавливает в качестве контекста выполнения this предоставленное значение. В метод также передаётся набор аргументов, которые будут установлены перед переданными в привязанную функцию аргументами при её вызове.

```js
func.bind(context, arg1, arg2, ...)
```

### Потеря «this»

```js
const someObject = {
    someString: 'Hello, I\'m pretty string ;)',
    someNumber: 30,
    isItAwesomeObject: true,
    logAll,
}

setTimeout(user.sayHi, 1000); // Привет, undefined!
```

