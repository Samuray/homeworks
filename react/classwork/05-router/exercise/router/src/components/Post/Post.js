import React from 'react';
import PropTypes from "prop-types";
import styles from './PostsContainer.module.scss';
import {Link, useHistory} from "react-router-dom";

const Post = (props) => {
        const { userId, title, body, id } = props;
        const history = useHistory();

        console.log('history', history);
        return (
            <div className={styles.root} onClick={() => history.push(`posts/${id}`)}>
                <span>User: {userId}</span>
                <h3>{title}</h3>
                <p>{body}</p>
            </div>
        );
};

Post.propTypes = {
        userId: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.number,
        ]),
        title: PropTypes.string.isRequired,
        body: PropTypes.string.isRequired,
}

Post.defaultProps = {
        userId: 1,
}

export default Post;
