
const promise1 = new Promise(resolve => {
    setTimeout(() => resolve('Hello from Promise'), 3000);
})

const promise2 = Promise.resolve(21);

const promise3 = new Promise((resolve, reject) => {
    fetch('https://catfact.ninja/fact')
        .then(response => response.json())
        .then(data => resolve(data))
        .catch(error => reject(error));
})

Promise.all([promise1, promise2, promise3]).then(data => console.log(data));