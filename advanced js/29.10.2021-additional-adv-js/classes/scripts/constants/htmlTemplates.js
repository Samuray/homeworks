export const htmlTemplateParagraph = `
    <input name="text" placeholder="Текст"></input>
    <input name="id" placeholder="id"></input>
    <input name="classes" placeholder="Классы"></input>
`;

export const htmlTemplateButton = `
    <input name="text" placeholder="Текст"></input>
    <input name="id" placeholder="id"></input>
    <input name="classes" placeholder="Классы"></input>
    <input name="type" placeholder="Тип"></input>
`;

export const htmlTemplateInput = `
    <input name="id" placeholder="id"></input>
    <input name="name" placeholder="Имя"></input>
    <input name="classes" placeholder="Классы"></input>
    <input name="type" placeholder="Тип"></input>
    <input name="placeholder" placeholder="placeholder"></input>
`;

export const htmlTemplateImage = `
    <input name="id" placeholder="id"></input>
    <input name="classes" placeholder="Классы"></input>
    <input name="src" placeholder="src"></input>
    <input name="alt" placeholder="alt"></input>
    <input name="width" placeholder="width"></input>
    <input name="height" placeholder="height"></input>
`;
