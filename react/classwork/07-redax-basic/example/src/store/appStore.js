import { createStore, applyMiddleware } from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import appReducer from "./reducers";
import thunk from "redux-thunk";


const appStore = createStore(appReducer, composeWithDevTools(applyMiddleware(thunk)));

export default appStore