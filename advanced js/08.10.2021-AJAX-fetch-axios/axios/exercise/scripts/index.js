const url = 'https://ajax.test-danit.com/api/swapi/vehicles';

axios.get(url).then(({ status, data }) => {
    const container = document.querySelector(".container");
    if(status === 200){
        console.log(data)
        data.forEach(({name, model, films,id}) =>{
            container.insertAdjacentHTML("beforeend",
                `<div class="card">
                    <p>Name: ${name}</p>
                    <p>Model: ${model}</p>
                    <ul id="vehicles-${id}"></ul>
                    </div>`)
            films.forEach(filmUrl=>{
                // Вставить прелоадер
                axios.get(filmUrl).then(({status, data:{name}}) => {
                    // Убрать прелоадер
                    if(status === 200){
                        document.querySelector(`#vehicles-${id}`)
                            .insertAdjacentHTML("beforeend", `<li>${name}</li>`)
                    }
                }).catch(error => console.log(error))
            })
        })
    }

}).catch(error => console.log(error));