import { Component } from "react";
import style from "./CardList.module.scss";
import Card from "../Card/Card";
import PropTypes from "prop-types";

 class CardList extends Component {
  state = {
    cards: JSON.parse(localStorage.getItem("cards")) || [],
    favorites: JSON.parse(localStorage.getItem("favorites")) || [],
  };

  addToCart = (code) => {
    let cards = [...this.state.cards, code];
    this.setState({ cards: cards });
    localStorage.setItem("cards", JSON.stringify(cards));
  };

  selectFavorite = (isFavorites) => {
    this.setState({ favorites: isFavorites });
    localStorage.setItem("favorites", JSON.stringify(isFavorites));
  };

  render() {
    return (
      <div className={`${style.wrapper} container`}>
        {this.props.productList.map((product) => {
          return (
            <Card
              key={product.id}
              urlImg={product.imgUrl}
              alt={product.productName}
              productName={product.productName}
              code={product.vendorСod}
              color={product.color}
              price={product.price}
              infoTitle={product.productName}
              addToCart={this.addToCart}
              favoritesArr={this.state.favorites}
              onClickSetFavorites={this.selectFavorite}
            />
          );
        })}
      </div>
    );
  }
}

Card.propTypes = {
  productList: PropTypes.array,
};

 export default CardList;