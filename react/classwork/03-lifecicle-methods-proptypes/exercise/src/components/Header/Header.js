import React, { PureComponent } from 'react';
import styles from './Header.module.scss';
import PropTypes from 'prop-types';

class Header extends PureComponent {
    render(){
        const { title, user: { name, age, avatar } } = this.props;

        return (
              <header className={styles.root}>
                  <span>{title}</span>
                  <div className={styles.userContainer}>
                      <img src={avatar} alt={`user ${name}`} />
                      <span>{name}, {age}</span>
                  </div>
              </header>
        );
    }
};

Header.propTypes = {
    title: PropTypes.string ,
    user: PropTypes.shape(
        {
            name: PropTypes.string,
            age: PropTypes.number,
            avatar: PropTypes.string
        }
    )
}

Header.defaultProps = {
    title: "Hello",
    user: {
        name:"Marina",
        age: 25,
        avatar: ""
    }
}
export default Header;
