
import { Component } from "react";
import style from "./App.module.scss";
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize";
import Navbar from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import CardList from "./components/CardList/CardList";
import Preloader from "./components/Preloader/Preloader";

 class App extends Component {
  state = {
    cards: [],
    isLoading: false,
    error: null,
  };

  componentDidMount() {
    this.setState({ isLoading: true });
    fetch("product.json")
        .then((res) => {
          if (res.ok) {
            return res.json();
          } else {
            throw new Error("Error to loads");
          }
        })
        .then((card) => {
          this.setState({ cards: card, isLoading: false });
        })
        .catch((e) => {
          this.setState({ error: e.message, isLoading: false });
        });
  }

  render() {
    const { cards, isLoading, error } = this.state;
    return (
        <div className={style.App}>
          <header>
            <Navbar />
          </header>
          <main className={style.main}>
            {isLoading && <Preloader />}
            {error && <div>{error}</div>}
            {cards && !cards.length ? (
                <div>{"Product list empty!!!"}</div>
            ) : (
                <CardList productList={cards} />
            )}
          </main>
          <footer>
            <Footer />
          </footer>
        </div>
    );
  }
}

export default App;
