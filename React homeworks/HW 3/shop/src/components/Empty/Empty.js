import style from "./Empty.module.scss";
import { NavLink } from "react-router-dom";
import React from "react";

const Empty = ({text}) => {
    return (<div className={`${style.emptyWrapper} `}>
            <div className={`${style.empty} center`}>{text}</div>
            <NavLink to='/'  className={`${style.toHome} center-align`}>
                <i className="material-icons "> </i>
                Back to home page
                <i className="material-icons">done</i>
            </NavLink>
        </div>
    )
}
export default Empty;
