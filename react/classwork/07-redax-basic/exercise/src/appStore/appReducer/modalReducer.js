import {SET_CONFIG_MODAL, SET_IS_OPEN_MODAL} from "../actions/actions";
const initialState = {
    isOpen: false,
    title: "",
    actions: null,
};

export const modalReducer = (state = initialState, action) =>{
    switch (action.type){
        case SET_CONFIG_MODAL:
            return {...state, actions: action.payload.actions, title: action.payload.title}
        case SET_IS_OPEN_MODAL:
            return {...state, isOpen: action.payload}
        default:{
            return state
        }
    }
}