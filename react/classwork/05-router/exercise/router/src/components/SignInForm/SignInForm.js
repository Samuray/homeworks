import React, {useState} from "react";
import Button from "../Button";
import styles from './SignInForm.module.scss'
import {useHistory} from "react-router-dom";


const SignInForm = () => {
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const history = useHistory();

    const testUser = {
        name: 'test',
        password: '123'
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        if (name === testUser.name && password === testUser.password) {
            localStorage.setItem('isAuth', 'true');
            history.push('/home');
        } else {
            setError('Invalid name or password')
        }
    }

    const handleChange = (value, name) => {
        if (error) setError('');
        const valuesMap = {
            name: () => setName(value),
            password: () => setPassword(value),
        }

        valuesMap[name]?.();
    }


    return (
        <form onSubmit={handleSubmit}>
            <input
                type="text"
                name="name"
                placeholder="Name"
                value={name}
                onChange={({ target: { value, name } }) => handleChange(value, name)}
            />
            <input
                type="text"
                name="password"
                placeholder="Password"
                value={password}
                onChange={({ target: { value, name } }) => handleChange(value, name)}
            />
            <p className={styles.error}>{error}</p>
            <Button type="submit">Log In</Button>
        </form>
    )
}

export default SignInForm;